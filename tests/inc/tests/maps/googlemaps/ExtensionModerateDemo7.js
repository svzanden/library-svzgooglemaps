dojo.provide('tests.maps.googlemaps.ExtensionModerateDemo7');

/**
 * Example of initiating your custom extensions
 * 
 */ 
dojo.declare('tests.maps.googlemaps.ExtensionModerateDemo7', null,
{
  /**
   * Constructor
   * 
   * @param object mapInstance
   * @return void
   */
    constructor: function(mapInstance)
    {
        this._mapInstance                 = mapInstance;

        this._panoramaDiv         = dojo.byId('panorama');
        this._panoramaSettings    = dojo.byId('panoramaSettings');
        this._polygonSettings     = dojo.byId('polygonSettings');
        this._autoRotating        = false;
        this._autoRotateHandler   = null;

        var buttonAutoRotate      = dojo.byId('panoramaStoredAutoRotate'),
            buttonFront           = dojo.byId('panoramaStoredViewFront'),
            buttonBack            = dojo.byId('panoramaStoredViewBack'),
            buttonLeft            = dojo.byId('panoramaStoredViewLeft');

        dojo.connect(buttonAutoRotate, 'onclick', this, '_onStoredAutoRotateButtonClick');
        dojo.connect(buttonFront, 'onclick', this, '_onStoredViewFrontButtonClick');
        dojo.connect(buttonBack, 'onclick', this, '_onStoredViewBackButtonClick');
        dojo.connect(buttonLeft, 'onclick', this, '_onStoredViewLeftButtonClick');

        this._triangleCoords      = [];
        this._bermudaTriangle     = null;
        this._MouseDownPos      = null;
        this._ALatLng           = null;
        this._Moving            = null;

        // Connecting to the map loaded event
        dojo.connect(this._mapInstance, 'onMapLoaded', this, 'onMapLoaded');

    },
    
    /**
     * Method which is called after the map is loaded
     * 
     * @param void
     * @return void
     */
    onMapLoaded: function()
    {
        var geocode = this._mapInstance.getCenter();
        var point   = geocode.getGoogleMapsPoint();

        var panoramaOptions = {
                position: point,
                pov: {
                    heading: 34,
                    pitch: 10,
                    zoom: 1
                }
        };

        this._panorama = new google.maps.StreetViewPanorama(this._panoramaDiv, panoramaOptions);
        this._mapInstance.getMap().setStreetView(this._panorama);

        dojo.connect(this._panorama, 'pano_changed', this, '_onPanoramaChanged');
        dojo.connect(this._panorama, 'pov_changed', this, '_onPanoramaChanged');

        // Point left bottom
        this._triangleCoords[0] = new google.maps.LatLng(52.618842, 4.751142);

        // Point right bottom
        this._triangleCoords[1] = new google.maps.LatLng(52.618971, 4.751245);

        // Point right top
        this._triangleCoords[2] = new google.maps.LatLng(52.619091, 4.750674);

        // Point left top
        this._triangleCoords[3] = new google.maps.LatLng(52.618971, 4.750674);

        this._polygonOptions = {
                strokeColor: "#FF0000",
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: "#FF0000",
                fillOpacity: 0.35
        };

        // Add a polygon
        // Construct the polygon
        this._bermudaTriangle = new google.maps.Polygon(this._polygonOptions);
        this._bermudaTriangle.setPaths(this._triangleCoords);

        console.log(this._panorama);

        this._bermudaTriangle.setMap(this._mapInstance.getMap());
        
        google.maps.event.addListener(this._bermudaTriangle, 'mousedown', dojo.hitch(this, this._onPolyMouseDown));
        google.maps.event.addListener(this._bermudaTriangle, 'mouseover', dojo.hitch(this, this._onPolyMouseOver));
        google.maps.event.addListener(this._bermudaTriangle, 'mouseout', dojo.hitch(this, this._onPolyMouseOut));
        google.maps.event.addListener(this._mapInstance.getMap(), 'mousemove', dojo.hitch(this, this._onMapMouseMove));
        google.maps.event.addDomListener(this._mapInstance.getMap().getDiv(), 'mouseup', dojo.hitch(this, this._onMapMouseUp));

        // Add custom tiles to registery
        //var map = this._mapInstance.getMap();

        //map.setStreetView(null); 

        console.log(map);
    },
    
    _onMapMouseUp: function(event)
    {
      this._Moving = false;
        
        var settings = '';
        
        for (var i = 0; i < 4; i++)
        {
            settings += 'Point ' + (i+1) + ': latitude: ' + this._triangleCoords[i].lat() + ' longitude: ' + this._triangleCoords[i].lng() + '<br />';      
        }

        this._polygonSettings.innerHTML = settings;
    },
    
    _onPolyMouseOver: function(event)
  {        
        this._polygonOptions.fillColor = '#FF6600';
        
        this._bermudaTriangle.setOptions(this._polygonOptions);
    },
    
    _onPolyMouseOut: function(event)
  {    
    this._polygonOptions.fillColor = '#FF0000';
    
    this._bermudaTriangle.setOptions(this._polygonOptions);
  },
    
    _onMapMouseMove: function(event)
    {
      if (event.cancelable) { event.preventDefault(); } 
      if (window.event) { window.event.returnValue = false; } 
      var newlatlng = event.latLng;
      if (this._Moving)
      {
        for (var i = 0; i < 3; i++)
        {
          var NewVertexPos = new google.maps.LatLng(this._ALatLng[i]["lat"] + newlatlng.lat() - this._MouseDownPos.lat(),this._ALatLng[i]["lng"] + newlatlng.lng() - this._MouseDownPos.lng());
          this._triangleCoords[i] = NewVertexPos;
        }
        this._bermudaTriangle.setPath(this._triangleCoords);
      }
    },
    
    _onPolyMouseDown: function(event)
    {
        console.log('OnPolyMouseDown');
        
      if (event.cancelable) { event.preventDefault(); } 
      if (window.event) { window.event.returnValue = false; }
        
         
      this._Moving = true;
      this._MouseDownPos = event.latLng;
      this._ALatLng = [];
        
      for (var i = 0; i < 3; i++)
      {
        this._ALatLng[i] = [];
        this._ALatLng[i]["lat"] = this._triangleCoords[i].lat();
        this._ALatLng[i]["lng"] = this._triangleCoords[i].lng();
      }
    },
    
    /**
     * Method onPanoramaChanged which is fired when the panorama is changed
     * 
     * @param {Void}
     * @return {Void}
     */
    _onPanoramaChanged: function()
    {        
        var pov = this._panorama.getPov();
        var point = this._panorama.getPosition();
        
        var settingsDiv = dojo.create('div');
        settingsDiv.innerHTML = 'Heading: ' + pov.heading + '<br />' +
                                'Pitch: ' + pov.pitch + '<br />' +
                                                        'Zoom: ' + pov.zoom + '<br />' + 
                                                        'Lat:' + point.lat() + '<br />' + 
                                                        'Lng:' + point.lng() + '<br />';
                
        dojo.place(settingsDiv, this._panoramaSettings, 'only');
        
    },
    
    /**
     * Method _onStoredAutoRotateButtonClick
     * 
     * @param {Object} event
     * @return {Void}
     */
    _onStoredAutoRotateButtonClick: function(event)
    {
        dojo.stopEvent(event);

        if (this._autoRotating)
        {
            console.log('Already auto rotating, disabling it');

            clearInterval(this._autoRotateHandler);
            this._autoRotating = false;

            event.target.innerHTML = 'Start auto rotating';

            return;
        }

        event.target.innerHTML = 'Stop auto rotating';

        this._autoRotating = true;

        var intervalHandler = dojo.hitch(this, function(speed)
        {
            this.rotate();
        });

        this._autoRotateHandler = setInterval(intervalHandler, 1);
    },
    
    /**
     * Method rotate
     * 
     * @param {Void}
     * @return {Void}
     */
    rotate: function()
    {
        var pov = this._panorama.getPov();
        pov.heading += 0.25;
        this._panorama.setPov(pov);
    },

    /**
   * Method _onStoredViewFrontButtonClick
   * 
   * @param {Object} event
   * @return {Void}
   */
  _onStoredViewFrontButtonClick: function(event)
  {
    dojo.stopEvent(event);
    
    console.log('Switching to front!');
    
    var pov = {
      heading: -49.51999999999999,
      pitch: 17.020000000000003,
      zoom: 1
    };
    
    var geocode = new svzsolutions.generic.Geocode(52.618898, 4.751187);
    
    this._panorama.setPov(pov);
    
    this._panorama.setPosition(geocode.getGoogleMapsPoint());
  },
  
  /**
   * Method _onStoredViewBackButtonClick
   * 
   * @param {Object} event
   * @return {Void}
   */
  _onStoredViewBackButtonClick: function(event)
  {
    dojo.stopEvent(event);
    
    console.log('Switching to back!');
    
    var pov = {
      heading: 57.670000000000016,
      pitch: 6.0400000000000045,
      zoom: 3
    };
    
    var geocode = new svzsolutions.generic.Geocode(52.618805, 4.750518);
    
    this._panorama.setPov(pov);
    
    this._panorama.setPosition(geocode.getGoogleMapsPoint());
  },
  
  /**
   * Method _onStoredViewLeftButtonClick
   * 
   * @param {Object} event
   * @return {Void}
   */
  _onStoredViewLeftButtonClick: function(event)
  {
    dojo.stopEvent(event);
    
    console.log('Switching to left!');
    
    var pov = {
      heading: -6.859999999999996,
      pitch: 6.0400000000000045,
      zoom: 1
    };
    
    var geocode = new svzsolutions.generic.Geocode(52.618649, 4.75099);
    
    this._panorama.setPov(pov);
    
    this._panorama.setPosition(geocode.getGoogleMapsPoint());
  }
});