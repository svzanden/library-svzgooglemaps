dojo.provide('tests.maps.googlemaps.ExtensionCustomMapImageTest1');

/**
 * Example of initiating your custom extensions
 * 
 */ 
dojo.declare('tests.maps.googlemaps.ExtensionCustomMapImageTest1', null,
{	
  _URL_REMOVE_MARKER:     '',
  _URL_STORE_MARKERS:     '',
  _URL_STORE_CENTER_ZOOM: '',
  
	/**
   * Constructor
   * 
   * @param object mapInstance
   * @return void
   */
	constructor: function(mapInstance, config)
	{
		this._mapInstance 				  = mapInstance;		
		
		this._information           = dojo.byId('information');
		this._addMarkerButton       = dojo.byId('add-marker-button');
		this._storeMarkerButton     = dojo.byId('store-marker-button');
		this._storeCenterZoomButton = dojo.byId('store-center-zoom-button');
		
		this._URL_REMOVE_MARKER     = config.urls.removeMarker;
		this._URL_STORE_MARKERS     = config.urls.storeMarker;
		this._URL_STORE_CENTER_ZOOM = config.urls.storeCenterZoom;
		
		
		var messageHolder         = dojo.query('.message-holder', dojo.body());
		var message               = dojo.query('.message', dojo.body());
		
		if (messageHolder && messageHolder[0])
		  this._messageHolder = messageHolder[0];
		
		if (message && message[0])
      this._message = message[0];
		
		// Connecting to the marker dragend event
		dojo.connect(this._mapInstance, 'onMapLoaded', this, '_onMapLoaded');
		dojo.connect(this._mapInstance, 'onCenterChanged', this, '_onCenterChanged');		
		dojo.connect(this._mapInstance, 'onInfoWindowContentLoaded', this, '_onInfoWindowContentLoaded');
		dojo.connect(this._addMarkerButton, 'onclick', this, '_onAddMarkerButtonClick');
		dojo.connect(this._storeMarkerButton, 'onclick', this, '_onStoreMarkerButtonClick');
		dojo.connect(this._storeCenterZoomButton, 'onclick', this, '_onStoreCenterZoomButtonClick');
	},
	
	/**
   * Method hideMessage which will hide the message 
   * 
   * @param void
   * @return void
   */
  hideMessage: function()
  {
    if (this._messageHolder && this._message)
      dojo.style(this._messageHolder, 'display', 'none');            

  },
	
	/**
	 * Method showMessage which will show a message 
	 * 
	 * @param string message
	 * @param string messageType
	 * @return void
	 */
	showMessage: function(message, messageType)
	{
	  if (this._messageHolder && this._message)
	  {
	    dojo.style(this._messageHolder, 'display', 'block');
	    dojo.addClass(this._messageHolder, messageType);
	    this._message.innerHTML = message;	    
	  }
	},
	
	/**
   * Method which is called after the map is loaded
   * 
   * @param void
   * @return void
   */
	_onMapLoaded: function()
	{											
	},
	
	/**
	 * Method fired when center changed
	 * 
	 * @param void
	 * @return void
	 */
	_onCenterChanged: function()
	{
	  var geocodeCenter = this._mapInstance.getCenter();
		
		if (geocodeCenter)
		{
			var settingsDiv = dojo.create('div');
	    settingsDiv.innerHTML = 'Latitude: ' + geocodeCenter.getLatitude() + '<br />' +
	                            'Longitude: ' + geocodeCenter.getLongitude() + '<br />';
	        
	    dojo.place(settingsDiv, this._information, 'only');
		}
	},
	
	/**
	 * Method onStoreMarkerButtonClick 
	 * 
	 * @param object event
	 * @return void
	 */
	_onStoreMarkerButtonClick: function(event)
	{
	  dojo.stopEvent(event);
				
		var markers     = this._mapInstance.getMarkers().getByType('admin');
		var numMarkers  = markers.length;
		var marker, markerInfo;
		var storeInfo   = [];
				
		for (var i = 0; i < numMarkers; i++)
		{
		  marker      = markers[i];
		  
		  // Prevent storing of already deleted markers
		  if (marker.isShown())
		  {
  		  markerInfo  = '';		  		  
  		  markerInfo += marker.getPosition().getLatitude() + '|' + marker.getPosition().getLongitude() + '|' + marker.getEntityId();		 
  		  
  		  storeInfo.push(markerInfo);
		  }
		}
		
		var xhrPost = {
      url: this._URL_STORE_MARKERS,
      handleAs: 'json',
      content: storeInfo,
      load: dojo.hitch(this, function(response, args)
      {                    
        if (response)
        {
          if (response.messageType == 'success')
            this.showMessage(response.message, response.messageType);
          else
            this.showMessage(response.message, response.messageType);           

        }  
        
      }),
      error: dojo.hitch(this, function()
      {
        this.showMessage('A general error occured while trying to do the store marker action.', 'error');
      })                  
    };
		
		this.hideMessage();
  
		dojo.xhrPost(xhrPost);		
	},
	
	/**
   * Method onStoreMarkerButtonClick 
   * 
   * @param object event
   * @return void
   */
  _onStoreCenterZoomButtonClick: function(event)
  {
    dojo.stopEvent(event);
    
    var storeInfo = {};
    
    storeInfo.zoomLevel = this._mapInstance.getZoomLevel();
    
    var centerPosition = this._mapInstance.getCenter();
    storeInfo.geocodeLatitude = centerPosition.getLatitude();
    storeInfo.geocodeLongitude = centerPosition.getLongitude();
    
    var xhrPost = {
      url: this._URL_STORE_CENTER_ZOOM,
      handleAs: 'json',
      content: storeInfo,
      load: dojo.hitch(this, function(response, args)
      {                    
        if (response)
        {
          if (response.messageType == 'success')
            this.showMessage(response.message, response.messageType);
          else
            this.showMessage(response.message, response.messageType);           

        }  
        
      }),
      error: dojo.hitch(this, function()
      {
        this.showMessage('A general error occured while trying to do the store center and zoomlevel action.', 'error');
      })                  
    };
    
    this.hideMessage();
  
    dojo.xhrPost(xhrPost);
  },
	
	/**
   * Method _onAddMarkerButtonClick which is fired when clicked upon the add marker button
   * 
   * @param object event
   * @return void
   */
  _onAddMarkerButtonClick: function(event)
  {
		dojo.stopEvent(event);
		
    var geocodeCenter = this._mapInstance.getCenter();
    
    if (geocodeCenter)
    {								
			var config = { 
			  draggable: true				 
			};
						
			var marker = new svzsolutions.generic.Marker(config, this._mapInstance.getMap());
			marker.setPosition(geocodeCenter);
			
			this._mapInstance.addMarker(marker, 'admin');									
    }
  },    
  
  /**
   * Method _onInfoWindowContentLoaded which is fired whenever an info window is being loaded
   * 
   * @param object infoWindow
   * @return void
   */
  _onInfoWindowContentLoaded: function(infoWindow)
  {
    if (infoWindow && infoWindow.content)
    {
      var content = infoWindow.content;
      
      var links = dojo.query('a', content);
      
      if (links && links[0])
      {
        var link;
        
        for (var i = 0; i < links.length; i++)
        {
          link = links[i];
          
          if (dojo.hasClass(link, 'marker-remove'))
          {
            dojo.connect(link, 'onclick', this, function()
            {
              var marker    = infoWindow.getMarker();
              
              var entityId  = marker.getEntityId();
              
              if (entityId > 0)
              {
                // Remove using ajax request
                //console.log('Remove using ajax request');

                var content = { entityId: entityId };
                
                var xhrPost = {
                  url: this._URL_REMOVE_MARKER,
                  handleAs: 'json',
                  content: content,
                  load: dojo.hitch(function(response, args)
                  {                    
                    if (response)
                    {
                      if (response.messageType == 'success')
                      {
                        infoWindow.destroy();
                        marker.destroy();
                      }
                      else
                      {
                        var messageHolder = dojo.query('.sg-message-holder', infoWindow.content);
                        
                        if (messageHolder && messageHolder[0])
                        {
                          dojo.style(messageHolder[0], 'display', 'block');
                        
                          var message = dojo.query('.message', messageHolder[0]);
                          
                          if (message && message[0])
                            message[0].innerHTML = response.message;
                        }
                       
                      }
                    }  
                    
                  }),
                  error: dojo.hitch(this, function()
                  {
                    this.showMessage('A general error occured while trying to do the delete marker action.', 'error');
                  })                  
                };
                
                this.hideMessage();
                
                dojo.xhrPost(xhrPost);                
              }
              else
              {
                // Is not stored so we can just remove it from the map
                infoWindow.destroy();
                marker.destroy();
              }              
               
            });
          }
        }
      }
      
    }
  }
});