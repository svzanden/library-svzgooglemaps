dojo.provide('tests.maps.googlemaps.ExtensionCustomMapImageTest1Map');

/**
 * Example of initiating your custom extensions
 * 
 */ 
dojo.declare('tests.maps.googlemaps.ExtensionCustomMapImageTest1Map', null,
{	
	/**
   * Constructor
   * 
   * @param object mapInstance
   * @return void
   */
	constructor: function(mapInstance, config)
	{
		this._mapInstance 				= mapInstance;
		this._TILE_URL            = config.tileUrl;
		
		console.log(this._TILE_URL);
		
		// Connecting to the marker dragend event
		dojo.connect(this._mapInstance, 'onMapLoaded', this, 'onMapLoaded');				
	},
	
	/**
   * Method which is called after the map is loaded
   * 
   * @param void
   * @return void
   */
	onMapLoaded: function()
	{					
		// Add custom tiles to registery
		var map = this._mapInstance.getMap();		
				
		// Normalizes the tile URL so that tiles repeat across the x axis (horizontally) like the
    // standard Google map tiles.
    function getHorizontallyRepeatingTileUrl(coord, zoom, urlfunc) {
      var y = coord.y;
      var x = coord.x;
 
      // tile range in one direction range is dependent on zoom level
      // 0 = 1 tile, 1 = 2 tiles, 2 = 4 tiles, 3 = 8 tiles, etc
      var tileRange = 1 << zoom;
 
      // don't repeat across y-axis (vertically)
      if (y < 0 || y >= tileRange) {
        return null;
      }
 
      // repeat across x-axis
      if (x < 0 || x >= tileRange) {
        x = (x % tileRange + tileRange) % tileRange;
      }
 
      return urlfunc({x:x,y:y}, zoom);
    } 
		
    var tileUrl = this._TILE_URL;
		
		var mapType = {
      getTileUrl: function(coord, zoom) {
        return getHorizontallyRepeatingTileUrl(coord, zoom, function(coord, zoom) {
					var bound = Math.pow(2, zoom);
          return tileUrl + "" + zoom + "/" + coord.x + "/" + (bound - coord.y - 1) + '.png';
        });
      },
      tileSize: new google.maps.Size(256, 256),
      isPng: true,
      maxZoom: 4,
			minZoom: 1,
      /*radius: 3396200,*/
      name: 'Plattegrond'
    };
				
		map.mapTypes.set('floorplan', new google.maps.ImageMapType(mapType));
		map.setMapTypeId('floorplan');
				
	}
});