<div  class="map-holder" style="position: absolute;  margin-top: 17px; margin-left: 3px;" id="<?php echo $visitmap->getContainerId(); ?>"></div>

<div id="tab-group" class="tab-tutorial" style="position:absolute; margin-top:-5px; z-index: 200;" >
	<div id="tab-tutorial" dojoType="dijit.layout.TabContainer" tabPosition="top" style="width: 415px; height:67px;">

		<div id="tab-mapcontrols" dojoType="dijit.layout.ContentPane"  title="Google Map Controls">
		</div>		

		<div id="tab-address" dojoType="dijit.layout.ContentPane" selected="selected" title="Search By Address">

				<form method="post" name="searchbyname" onSubmit="return false;">			
					<div class="demo-settings style="font-size:75%;">
						<label for="map-center-geocode-address-search" style="font-size:75%;">Address: </label><input type="text" id="map-center-geocode-address-search" onkeypress="return enter(document.searchbyname.mysearch)" name="map-center-geocode-address-search" value="<?php echo $addressSearch; ?>" style="height:14px; font-size:90%; width:190px;" />
						<button type="button" id="mysearch" name="mysearch" style="height:20px; margin-top:1px; font-size:75%; margin-left:10px;">Search</button>
						<br><span style="font-size:75%;">Map Center: </span><span id="map-center-geocode-address-found" style="font-size:75%;"></span>		
					</div>	
				</form>


		</div>

		<div id="tab-coordinates" dojoType="dijit.layout.ContentPane"  title="Search by Coordinates">
			<div class="demo-settings">
				<form method="post">			
					<div id="myblock" style="margin-top:2px; "> 
						<div style="display:none;">
							<label for="map-center-zoom-level">Zoom level:</label>
							<select id="map-center-zoom-level" name="map-center-zoom-level">
								<?php
									$maxZoomLevel = 19;
									for ($i = 1; $i < $maxZoomLevel; $i++)
									{
										$selected = '';
										if ($i == $visitmap->getZoomLevel())
										$selected = ' selected="selected"';
										echo '<option value="' . $i . '"' . $selected . '>' . $i . '</option>';
									}
								?>
							</select>
							<label for="map-center-geocode-latitude-dms">Latitude DMS:</label><input type="text" id="map-center-geocode-latitude-dms" name="map-center-geocode-latitude-dms" value="<?php echo $visitmap->getCenterGeocode()->getLatitudeInDMS(); ?>" disabled="disabled" />
							<label for="map-center-geocode-longitude-dms">Longitude DMS:</label><input type="text" id="map-center-geocode-longitude-dms" name="map-center-geocode-longitude-dms" value="<?php echo $visitmap->getCenterGeocode()->getLongitudeInDMS(); ?>" disabled="disabled" />
						</div>

						<div style="font-size:75%;">
							<label for="map-center-geocode-latitude">Latitude: </label><input type="text" id="map-center-geocode-latitude" name="map-center-geocode-latitude" value="<?php echo $visitmap->getCenterGeocode()->getLatitude(); ?>" style="height:14px; font-size:90%; width:90px;" />
							<label for="map-center-geocode-longitude" style=" margin-left:10px; ">Longitude: </label><input type="text" id="map-center-geocode-longitude" name="map-center-geocode-longitude" value="<?php echo $visitmap->getCenterGeocode()->getLongitude(); ?>"  style="height:14px; font-size:90%; width:90px;" />
							<button type="button"  style="font-size:65%; height:19px; margin-left:10px;">Go</button>
						</div> 
					</div>
				</form>
			</div>
		</div>

	</div>
</div>



