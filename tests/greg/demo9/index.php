<?php session_start(); // start up your PHP SESSION!

   // Including of the SVZ Solutions library
//require($_SERVER['DOCUMENT_ROOT'].'/includes/svzsolutions/maps/Map.php');

  require_once(dirname(__FILE__) . '/../../../includes/svzsolutions/maps/Map.php');

  $mapClusterMode               = SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_DISTANCE;
  $mapWidth                     = 97;
  $mapHeight                    = 600;

 $mapCenterGeocodeLatitude     =  !empty($_SESSION['centerlat']) ? (float)str_replace("...", "", $_SESSION['centerlat']) : '';
if ( $mapCenterGeocodeLatitude == '' ){  $mapCenterGeocodeLatitude         =  !empty($_COOKIE['map_center_geocode_latitude']) ? (float)$_COOKIE['map_center_geocode_latitude'] : (float)-25; }

 $mapCenterGeocodeLongitude    =  !empty($_SESSION['centerlng']) ?(float)str_replace("...", "", $_SESSION['centerlng'])  : '';
if ( $mapCenterGeocodeLongitude    == '' ){  $mapCenterGeocodeLongitude    = !empty($_COOKIE ['map_center_geocode_longitude']) ?  (float)$_COOKIE['map_center_geocode_longitude'] : (float)134; }

$zoomLevel                 =  !empty($_SESSION['zoomLevel']) ? (integer)$_SESSION['zoomLevel'] : '';
if ( $zoomLevel                 == '' ){  $zoomLevel                       = !empty($_COOKIE['map_zoom_level'])                ? (integer)$_COOKIE['map_zoom_level']: (integer)5; }

  $mapTypeControlStyle          = 'default';
  $mapNavigationControlStyle    = 'default';
  $debugMode                    = isset($_GET['debugMode']) ? true : false;

  $centerGeocode                = new SVZ_Solutions_Generic_Geocode($mapCenterGeocodeLatitude, $mapCenterGeocodeLongitude);


   $visitmap                          = SVZ_Solutions_Maps_Map::getInstance(SVZ_Solutions_Maps_Map::MAP_TYPE_GOOGLE_MAPS, '3');
   $shortmap                          = SVZ_Solutions_Maps_Map::getInstance(SVZ_Solutions_Maps_Map::MAP_TYPE_GOOGLE_MAPS, '3');
  $visitmap->setWidth($mapWidth);
  $visitmap->setHeight($mapHeight);
  $shortmap->setWidth($mapWidth);
  $shortmap->setHeight($mapHeight);


  $addressSearch                = !empty($_POST['marker-geocode-address-search']) ? $_POST['marker-geocode-address-search'] : '';

  // Sets the id of the container (HTMLDomElement) the map must be put on.
$visitmap->setContainerId('visitmap'); // Provide the id of the div the map should be located on
$shortmap->setContainerId('shortmap'); // Provide the id of the div the map should be located on

 // Set the absolute or relative location of the file which will return your data like markers.
  $visitmap->setDataLoadUrl('data.php'); // Set the absolute or relative location of the file which will return the markers
  $shortmap->setDataLoadUrl('data.php'); // Set the absolute or relative location of the file which will return the markers

$visitmap->setMapType('roadmap');
$visitmap->setZoomLevel($zoomLevel);

$visitmap->setCenterGeocode($centerGeocode);
$visitmap->setMapTypeControlStyle($mapTypeControlStyle);
$visitmap->setMapNavigationControlStyle($mapNavigationControlStyle);

$shortmap->setMapType('roadmap');
$shortmap->setZoomLevel($zoomLevel);

$shortmap->setCenterGeocode($centerGeocode);
$shortmap->setMapTypeControlStyle($mapTypeControlStyle);
$shortmap->setMapNavigationControlStyle($mapNavigationControlStyle);

 // Setting the cluster types (related to the current test data)
  $typeScenarios = array(
    '101', '102', '103', '104', '105', '106', '107', '108', '109', '110', '111', '112', '113',
  );

  // Create for each scenario a marker type
  foreach ($typeScenarios as $scenario)
  {
    // Add a scenario marker type
    $markerType = new SVZ_Solutions_Generic_Marker_Type($scenario);
    $visitmap->addMarkerType($markerType);
 	$shortmap->addMarkerType($markerType);

  }

  // Add a cluster marker type
  $markerType = new SVZ_Solutions_Generic_Marker_Type(SVZ_Solutions_Generic_Marker_Type::MARKER_TYPE_CLUSTER);
  $markerType->setClickAction('zoom');

  $visitmap->addMarkerType($markerType);
  $shortmap->addMarkerType($markerType);

  // Add a list marker type
  $markerType = new SVZ_Solutions_Generic_Marker_Type(SVZ_Solutions_Generic_Marker_Type::MARKER_TYPE_LIST);
  $visitmap->addMarkerType($markerType);
  $shortmap->addMarkerType($markerType);

?>




<!DOCTYPE HTML>
	<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
		<head>
 			 <meta charset="utf-8" />
  			<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />

			<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/dojo/1.5.0/dijit/themes/tundra/tundra.css" />



<script type="text/javascript">
  // <![CDATA[
    var djConfig = {
    parseOnLoad: true,
    cacheBust: '<?php echo SVZ_Solutions_Maps_Map::APP_VERSION; ?>',
    baseUrl: "./",
    modulePaths: { svzsolutions: "../../../inc/svzsolutions", yourlib: "../../../inc/yourlib" }
    };
  // ]]>
  </script>


  <script src="http://ajax.googleapis.com/ajax/libs/dojo/1.5.0/dojo/dojo.xd.js"></script>

			<script type="text/javascript">
				dojo.require("dijit.layout.TabContainer");
				dojo.require("dijit.layout.ContentPane");
				dojo.require("dijit.layout.AccordionContainer");
				dojo.require('dijit.Dialog');
				dojo.require("dijit.Tooltip");
			</script>


  <script type="text/javascript">
  // <![CDATA[

    var visitmap = null;
    var shortmap = null;
    /**
     * Function which is called after the DOM has finished loading
     *
     * @param void
     * @return void
     */
    var init = function()
    {
      var mapManager  = new svzsolutions.maps.MapManager();

      // The SVZ_Solutions_Maps_Google_Maps_Map php class will generate a config object depending on your settings for you,
      // this generated object can be encoded into a JSON string and can be put encoded into the svzsolutions.maps.MapManager object.
	visitmap         = mapManager.initByConfig('<?php echo json_encode($visitmap->getConfig()); ?>');
	shortmap         = mapManager.initByConfig('<?php echo json_encode($shortmap->getConfig()); ?>');

      var extension1 	= new yourlib.maps.googlemaps.ExtensionAdvancedDemo2(visitmap);
      var extension2   	= new yourlib.maps.googlemaps.ExtensionModerateDemo5(visitmap);
      var extension3 	= new yourlib.maps.googlemaps.ExtensionAdvancedDemo2(shortmap);
      var extension4   	= new yourlib.maps.googlemaps.ExtensionAdvancedDemoResize(shortmap);
      var extension5   	= new yourlib.maps.googlemaps.ExtensionAdvancedDemoResize(visitmap);


      // Startup all the maps (call after subscribing within your extensions)
      mapManager.startup();
    };

    //dojo.require('svzsolutions.maps.MapManager');
    dojo.require('svzsolutions.all');
    dojo.require('yourlib.maps.googlemaps.ExtensionModerateDemo5');
    dojo.require('yourlib.maps.googlemaps.ExtensionAdvancedDemo2');
    dojo.require('yourlib.maps.googlemaps.ExtensionAdvancedDemoResize');
    dojo.addOnLoad(init);
  // ]]>
  </script>




</head>


<body class="tundra" >


<div class="tab-almanac" style="position: absolute; margin-top: 50px; margin-left: 5px; z-index: 100; padding-bottom: -5px;">
	<div id="tab-myprofile" dojoType="dijit.layout.TabContainer" tabPosition="top" >
				<form id="sg-filter" method="post" style="display:none;">
					<a><input class="input-marker-type" type="checkbox" id="marker-type-userid" name="marker-types-userid" checked="checked" value="test" /><label for="marker-type-userid">User Id</label></a>
				</form>



			<div id="tab-mytripplan" dojoType="dijit.layout.ContentPane" title="My Trip Plans">
				nonthing to see here, move along
			</div>

			<div id="tab-myvisits" dojoType="dijit.layout.ContentPane" title="My Visited" >
					<div style="width:90%; margin-top:5px; margin-left:10px;">
						Blurb Here
					</div>

				<div dojoType="dijit.layout.AccordionContainer" id="acc-visited" class="almanac" duration="400" orientation="vertical"   style="margin-left: -5px; ">
					<div id="tab-visited-acc-list" dojoType="dijit.layout.AccordionPane" selected="true" title="View in List">
						blurb here
					</div>

					<div dojoType="dijit.layout.AccordionPane"  title="View on Map" id="view-on-map">
						<?php require("tab-myvisited-acc-map.php"); ?>
					</div>


				</div>

			</div>

			<div id="tab-myshortlist" dojoType="dijit.layout.ContentPane" title="My Shortlist" >
					<div style="width:90%; margin-top:5px; margin-left:10px;">
						testing stuff here
					</div>

				<div dojoType="dijit.layout.AccordionContainer" id="acc-shortlist" class="almanac" duration="400" orientation="vertical"   style="margin-left: -5px; ">
					<div id="tab-shortlist-acc-list" dojoType="dijit.layout.AccordionPane" selected="true"  title="View in List" >
						more test goes here
					</div>

					<div dojoType="dijit.layout.AccordionPane" title="View on Map">
						<?php require("tab-myshortlist-acc-map.php"); ?>
					</div>


				</div>

			</div>







	</div>
</div>


<script type="text/javascript">

	var myWidth;
	var myHeight;

function GetWindowSize(){
	if( typeof( window.innerWidth ) == 'number' ) {
		//Non-IE
		myWidth = window.innerWidth;
		myHeight = window.innerHeight;
	} else if( document.documentElement &&
		( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
		//IE 6+ in 'standards compliant mode'
		myWidth = document.documentElement.clientWidth;
		myHeight = document.documentElement.clientHeight;
	} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
		//IE 4 compatible
		myWidth = document.body.clientWidth;
		myHeight = document.body.clientHeight;
	}
}



function SizeProfileMainWindow(){
	mySearchLeft = myWidth - 452;
	if(dojo.byId('tab-tutorial')) { dojo.byId("tab-tutorial").style.marginLeft = mySearchLeft+'px'; }
	if(dojo.byId('tab-tutorial')) { dojo.byId("tab-tutorial").style.heigh1t = '25px'; }
	if(dojo.byId('tab-tutorial-short')) { dojo.byId("tab-tutorial-short").style.marginLeft = mySearchLeft+'px'; }
	if(dojo.byId('tab-tutorial-short')) { dojo.byId("tab-tutorial-short").style.heigh1t = '25px'; }

	myTabWidth = myWidth - 20;
	myTabHeight = myHeight - 80;
	if(dojo.byId('tab-myprofile')) {	dojo.byId("tab-myprofile").style.width = myTabWidth+'px';}
	if(dojo.byId('tab-myprofile')) {	dojo.byId("tab-myprofile").style.height = myTabHeight+'px';}

	myACCHeight = myHeight - 180;
	myACCWidth = myWidth - 30;
	if(dojo.byId('acc-visited')) {	dojo.byId("acc-visited").style.width = myACCWidth+'px';}
	if(dojo.byId('acc-visited')) {	dojo.byId("acc-visited").style.height = myACCHeight+'px';}

	if(dojo.byId('acc-shortlist')) {	dojo.byId("acc-shortlist").style.width = myACCWidth+'px';}
	if(dojo.byId('acc-shortlist')) {	dojo.byId("acc-shortlist").style.height = myACCHeight+'px';}

	myMapWidth = myWidth - 80;
	myMapHeight = myHeight - 270;
	if(dojo.byId('visitmap')) {	dojo.byId("visitmap").style.height = myMapHeight+'px';}
	if(dojo.byId('visitmap')) {	dojo.byId("visitmap").style.width = myMapWidth+'px';}
	if(dojo.byId('shortmap')) {	dojo.byId("shortmap").style.height = myMapHeight+'px';}
	if(dojo.byId('shortmap')) {	dojo.byId("shortmap").style.width = myMapWidth+'px';}
}



	GetWindowSize();
	SizeProfileMainWindow();

	window.onresize = function() {
		GetWindowSize();
		SizeProfileMainWindow();
		if(visitmap){visitmap.resize();}
		if(shortmap){shortmap.resize();}
	}
</script>

</body>
</html>