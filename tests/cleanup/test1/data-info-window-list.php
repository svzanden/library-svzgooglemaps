<?php

  /**
   * Copyright (c) 2009, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Advanced demo 1
   * @description:  This demo data file retrieves the content for a info window list.
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.2
   * @versionDate:  2010-02-06
   * @date:         2010-02-06
   */

  require_once('data-info-window.php');

?>