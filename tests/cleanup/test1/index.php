<?php

  /**
   * Copyright (c) 2009, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Advanced demo 1
   * @description:  This tests shows the creating and removing maps and cleaning up the memory used.
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.2
   * @versionDate:  2010-02-13
   * @date:         2010-02-13
   */

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/maps/Map.php');

  $map                          = SVZ_Solutions_Maps_Map::getInstance(SVZ_Solutions_Maps_Map::MAP_TYPE_GOOGLE_MAPS, '3');
  $map->setWidth(900);
  $map->setHeight(600);

  // Sets the id of the container (HTMLDomElement) the map must be put on.
  $map->setContainerId('map');

  // Set the absolute or relative location of the file which will return your data like markers.
  $map->setDataLoadUrl('data.php');

  // Sets the default map type to roadmap.
  $map->setMapType('roadmap');

  // Sets the zoom level to start with to 8.
  $map->setZoomLevel(8);

  // Sets the geocode the map should start at centered.
  $map->setCenterGeocode(new SVZ_Solutions_Generic_Geocode(51.5479672, 5.648499));

  // Create for each type a marker type
  foreach ($types as $type)
  {
    // Add a type marker type
    $markerType = new SVZ_Solutions_Generic_Marker_Type($type);
    $map->addMarkerType($markerType);
  }

  // Add a cluster marker type
  $markerType = new SVZ_Solutions_Generic_Marker_Type(SVZ_Solutions_Generic_Marker_Type::MARKER_TYPE_CLUSTER);
  $markerType->setClickAction('zoom');

  $map->addMarkerType($markerType);

  // Add a list marker type
  $markerType = new SVZ_Solutions_Generic_Marker_Type(SVZ_Solutions_Generic_Marker_Type::MARKER_TYPE_LIST);
  $map->addMarkerType($markerType);

?>
<!DOCTYPE HTML>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>SVZ Solutions - Maps - Cleanup test 1</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
  <link rel="stylesheet" type="text/css" href="../../../inc/css/svzmaps.css" media="all" />
  <link rel="stylesheet" type="text/css" href="../../../demos/inc/css/svzmaps-your.css" media="all" />

  <!--[if lte IE 9]>
  <link rel="stylesheet" type="text/css" href="../../../demos/inc/css/svzmaps-your-ie.css" media="all" />
  <![endif]-->

  <!-- BEGIN: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->
  <link rel="stylesheet" type="text/css" href="../../../demos/inc/css/demo.css" media="all" />
  <!-- END: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->

  <script type="text/javascript">
  // <![CDATA[
    var djConfig = {
    parseOnLoad:true,
    cacheBust: '<?php echo SVZ_Solutions_Maps_Map::APP_VERSION; ?>'
    };
  // ]]>
  </script>
  <script type="text/javascript" src="../../../inc/dojotoolkit/dojo/dojo.js"></script>
  <script type="text/javascript">
  // <![CDATA[

    /**
     * Function which is called after the DOM has finished loading
     *
     * @param void
     * @return void
     */
    var init = function()
    {
      // Make sure require of the svz javascript library and your extensions (if any) is done after the google library has been loaded
      dojo.require('svzsolutions.maps.MapManager');

      var mapManager = new svzsolutions.maps.MapManager();

      // The SVZ_Solutions_Maps_Google_Maps_Map php class will generate a config object depending on your settings for you,
      // this generated object can be encoded into a JSON string and can be put encoded into the svzsolutions.maps.MapManager object.
      var config = mapManager.loadByConfig('<?php echo json_encode($map->getConfig()); ?>');

      dojo.query('.unload-map', dojo.body()).connect('onclick', function(event)
          {
            dojo.stopEvent(event);

            console.log('unload map');

            mapManager.destroyByIndex(config.mapIndex);
          });

      dojo.query('.load-new-map', dojo.body()).connect('onclick', function(event)
          {
            dojo.stopEvent(event);

            console.log('load new map');

            mapManager = new svzsolutions.maps.MapManager();

            var mapContainer = dojo.create('div', { id: '<?php echo $map->getContainerId(); ?>', className: 'map-holder', style: { width: '<?php echo $map->getWidth(); ?>px', height: '<?php echo $map->getHeight(); ?>px' } }, dojo.body());

            config = mapManager.loadByConfig('<?php echo json_encode($map->getConfig()); ?>');
          });
    }

    dojo.addOnLoad(init);
  // ]]>
  </script>
</head>
<body>

  <div class="demo-information">
    <h1>Cleanup test 1: Test of creating and removing maps and cleaning up the memory used.</h1>
    <p><a href="../../index.html">&#171; back to test overview</a></p>
    <p>This tests shows the creating and removing maps and cleaning up the memory used.</p>
    <p><a class="unload-map" href="#">Unload map</a> | <a class="load-new-map" href="#">Load new map</a></p>
  </div>

  <div id="<?php echo $map->getContainerId(); ?>" class="map-holder" style="width: <?php echo $map->getWidth(); ?>px; height: <?php echo $map->getHeight(); ?>px"></div>

</body>
</html>