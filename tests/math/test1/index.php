<?php

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/generic/MarkerManager.php');


  echo '1 kilometres = ' . SVZ_Solutions_Generic_Math::kilometresToMiles(1) . ' miles<br />';
  echo '1.5 kilometres = ' . SVZ_Solutions_Generic_Math::kilometresToMiles(1.5) . ' miles<br />';
  echo '1 miles = ' . SVZ_Solutions_Generic_Math::milesToKilometres(1) . ' kilometres<br />';
  echo '1.5 miles = ' . SVZ_Solutions_Generic_Math::milesToKilometres(1.5) . ' kilometres<br />';


  $geocode  = new SVZ_Solutions_Generic_Geocode(53.754842, -2.708077);
  $result   = SVZ_Solutions_Generic_Math::getRadiusByGeocode($geocode, 20);

  print_r($result);

  echo '<br /><br />';

  $point1 = new SVZ_Solutions_Generic_Geocode(52.347968, 5.6484991); // Lammertkamp 30-48, 3848 Harderwijk, Nederland
  $point2 = new SVZ_Solutions_Generic_Geocode(52.34801034091123, 5.647200810836783); // Lammertkamp 20, 3848 Harderwijk, Nederland

  echo 'Distance = ' . SVZ_Solutions_Generic_Math::getDistanceInKilometres($point1, $point2) . ' kilometres (result as returned by Google is 88metre)<br />';
  echo 'Distance = ' . SVZ_Solutions_Generic_Math::getDistanceInMiles($point1, $point2) . ' miles<br />';

  echo 'Miles result from previous distance = ' . SVZ_Solutions_Generic_Math::milesToKilometres(SVZ_Solutions_Generic_Math::getDistanceInMiles($point1, $point2)) . ' kilometres<br />';

  echo 'Distance in pixels on zoom level 21 = ' . SVZ_Solutions_Generic_Math::getDistanceInPixels($point1, $point2, 21) . ' pixels<br />';
  echo 'Distance in pixels on zoom level 20 = ' . SVZ_Solutions_Generic_Math::getDistanceInPixels($point1, $point2, 20) . ' pixels<br />';
  echo 'Distance in pixels on zoom level 19 = ' . SVZ_Solutions_Generic_Math::getDistanceInPixels($point1, $point2, 19) . ' pixels (this should be 484px verified by print screen)<br />';
  echo 'Distance in pixels on zoom level 18 = ' . SVZ_Solutions_Generic_Math::getDistanceInPixels($point1, $point2, 18) . ' pixels (this should be 242px verified by print screen)<br />';
  echo 'Distance in pixels on zoom level 17 = ' . SVZ_Solutions_Generic_Math::getDistanceInPixels($point1, $point2, 17) . ' pixels (this should be 121px verified by print screen)<br />';
  echo 'Distance in pixels on zoom level 12 = ' . SVZ_Solutions_Generic_Math::getDistanceInPixels($point1, $point2, 12) . '<br/>';
  echo 'Distance in pixels on zoom level 4 = ' . SVZ_Solutions_Generic_Math::getDistanceInPixels($point1, $point2, 4) . '<br/>';


  echo '<h2>Viewport tests</h2>';
  echo '<h3>Netherlands</h3>';

  // Caluclating the pixel distance between the left and right side of a view port
  $_GET['zoom']   = '8';
  $_GET['sw_lat'] = '50.51160236957179';
  $_GET['sw_lng'] = '3.1765751718749913';
  $_GET['ne_lat'] = '52.5612512282094';
  $_GET['ne_lng'] = '8.12042282812499';
  //$_GET['ce_lat'] = '51.5479672';
  //$_GET['ce_lng'] = '5.648499';

  $viewPort = SVZ_Solutions_Generic_ViewPort::getInstance(true);

  $distanceInPixels = SVZ_Solutions_Generic_Math::getDistanceInPixels($viewPort->getBounds()->getGeocodeNorthWest(), $viewPort->getBounds()->getGeocodeNorthEast(), $viewPort->getZoomLevel()) . '<br />';

  echo 'South west latitude = ' . $viewPort->getBounds()->getGeocodeSouthWest()->getLatitude() . ' and longitude = ' . $viewPort->getBounds()->getGeocodeSouthWest()->getLongitude() . '<br />';
  echo 'North east latitude = ' . $viewPort->getBounds()->getGeocodeNorthEast()->getLatitude() . ' and longitude = ' . $viewPort->getBounds()->getGeocodeNorthEast()->getLongitude() . '<br />';
  echo 'The calculated north west latitude = ' . $viewPort->getBounds()->getGeocodeNorthWest()->getLatitude() . ' (should be 52.5612512282094) and longitude = ' . $viewPort->getBounds()->getGeocodeNorthWest()->getLongitude() . ' (should be 3.1765751718749913)<br />';
  echo 'The calculated south east latitude = ' . $viewPort->getBounds()->getGeocodeSouthEast()->getLatitude() . ' (should be 50.51160236957179) and longitude = ' . $viewPort->getBounds()->getGeocodeSouthEast()->getLongitude() . ' (should be 8.12042282812499)<br />';
  echo 'The calculated center latitude = ' . $viewPort->getBounds()->getGeocodeCenter()->getLatitude() . ' (should be 51.5479672) and longitude = ' . $viewPort->getBounds()->getGeocodeCenter()->getLongitude() . ' (should be 5.648499)<br />';
  echo 'The calculated distance between the NE point and NW point = ' . $distanceInPixels  . ' pixels (should be 900)<br />';
  echo 'ViewPort query including radius = ' . $viewPort->getSqlQueryPart() . '<br />';

  echo '<h3>Australia overlapping to America</h3>';

  // Caluclating the pixel distance between the left and right side of a view port
  $_GET['zoom']   = '4';
  $_GET['sw_lat'] = '-60.73371756220644';
  $_GET['sw_lng'] = '146.48773239843746';
  $_GET['ne_lat'] = '-23.516256723846123';
  $_GET['ne_lng'] = '-134.4107051015625';
  //$_GET['ce_lat'] = '-45.052266845770966';
  //$_GET['ce_lng'] = '-173.9614863515625';

  $viewPort = SVZ_Solutions_Generic_ViewPort::getInstance(true);

  $distanceInPixels = SVZ_Solutions_Generic_Math::getDistanceInPixels($viewPort->getBounds()->getGeocodeNorthWest(), $viewPort->getBounds()->getGeocodeNorthEast(), $viewPort->getZoomLevel()) . '<br />';

  echo 'South west latitude = ' . $viewPort->getBounds()->getGeocodeSouthWest()->getLatitude() . ' and longitude = ' . $viewPort->getBounds()->getGeocodeSouthWest()->getLongitude() . '<br />';
  echo 'North east latitude = ' . $viewPort->getBounds()->getGeocodeNorthEast()->getLatitude() . ' and longitude = ' . $viewPort->getBounds()->getGeocodeNorthEast()->getLongitude() . '<br />';
  echo 'The calculated north west latitude = ' . $viewPort->getBounds()->getGeocodeNorthWest()->getLatitude() . ' (should be -23.516256723846123) and longitude = ' . $viewPort->getBounds()->getGeocodeNorthWest()->getLongitude() . ' (should be 146.48773239843746)<br />';
  echo 'The calculated south east latitude = ' . $viewPort->getBounds()->getGeocodeSouthEast()->getLatitude() . ' (should be -60.73371756220644) and longitude = ' . $viewPort->getBounds()->getGeocodeSouthEast()->getLongitude() . ' (should be -134.4107051015625)<br />';
  echo 'The calculated center latitude = ' . $viewPort->getBounds()->getGeocodeCenter()->getLatitude() . ' (should be -45.052266845770966) and longitude = ' . $viewPort->getBounds()->getGeocodeCenter()->getLongitude() . ' (should be -173.9614863515625)<br />';
  echo 'The calculated distance between the NE point and NW point = ' . $distanceInPixels  . ' pixels (should be 900)<br />';
  echo 'ViewPort query including radius = ' . $viewPort->getSqlQueryPart() . '<br />';

  echo '<h3>Right of russia overlapping canada</h3>';

  // Caluclating the pixel distance between the left and right side of a view port
  $_GET['zoom']   = '4';
  $_GET['sw_lat'] = '33.913632572045465';
  $_GET['sw_lng'] = '141.82952927343752';
  $_GET['ne_lat'] = '66.03917037391403';
  $_GET['ne_lng'] = '-139.0689082265625';
  //$_GET['ce_lat'] = '52.834225308115556';
  //$_GET['ce_lng'] = '-178.6196894765625';

  $viewPort = SVZ_Solutions_Generic_ViewPort::getInstance(true);

  $distanceInPixels = SVZ_Solutions_Generic_Math::getDistanceInPixels($viewPort->getBounds()->getGeocodeNorthWest(), $viewPort->getBounds()->getGeocodeNorthEast(), $viewPort->getZoomLevel()) . '<br />';

  echo 'South west latitude = ' . $viewPort->getBounds()->getGeocodeSouthWest()->getLatitude() . ' and longitude = ' . $viewPort->getBounds()->getGeocodeSouthWest()->getLongitude() . '<br />';
  echo 'North east latitude = ' . $viewPort->getBounds()->getGeocodeNorthEast()->getLatitude() . ' and longitude = ' . $viewPort->getBounds()->getGeocodeNorthEast()->getLongitude() . '<br />';
  echo 'The calculated north west latitude = ' . $viewPort->getBounds()->getGeocodeNorthWest()->getLatitude() . ' (should be 66.03917037391403) and longitude = ' . $viewPort->getBounds()->getGeocodeNorthWest()->getLongitude() . ' (should be 141.82952927343752)<br />';
  echo 'The calculated south east latitude = ' . $viewPort->getBounds()->getGeocodeSouthEast()->getLatitude() . ' (should be 33.913632572045465) and longitude = ' . $viewPort->getBounds()->getGeocodeSouthEast()->getLongitude() . ' (should be -139.0689082265625)<br />';
  echo 'The calculated center latitude = ' . $viewPort->getBounds()->getGeocodeCenter()->getLatitude() . ' (should be 52.834225308115556) and longitude = ' . $viewPort->getBounds()->getGeocodeCenter()->getLongitude() . ' (should be -178.6196894765625)<br />';
  echo 'The calculated distance between the NE point and NW point = ' . $distanceInPixels  . ' pixels (should be 900)<br />';
  echo 'ViewPort query including radius = ' . $viewPort->getSqlQueryPart() . '<br />';


?>