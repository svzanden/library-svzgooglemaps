<?php

  /**
   * Copyright (c) 2010, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions
   * @description:  This file tests if the query is proper generated when setting a custom radius for clustering
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.6
   * @versionDate:  2010-09-25
   * @date:         2010-09-25
   */

  // Including of some sample data
  //require_once('../../testdata/data.php');

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/generic/MarkerManager.php');

  // Facking a request
  $_GET['zoom']   = '8';
  $_GET['sw_lat'] = '50.51160236957179';
  $_GET['sw_lng'] = '3.1765751718749913';
  $_GET['ne_lat'] = '52.5612512282094';
  $_GET['ne_lng'] = '8.12042282812499';

  $viewPort = SVZ_Solutions_Generic_ViewPort::getInstance();
  $viewPort->setRadiusInPixels();

  echo $viewPort->getSqlQueryPart();

  // Reading out data applied in the requests
  /*$mapClusterMode   = SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_DISTANCE;

  $markerManager    = new SVZ_Solutions_Generic_Marker_Manager();
  $markerManager->setListDataLoadUrl('data-info-window-list.php');
  $markerManager->setClusterMode($mapClusterMode);
  $markerManager->import($data);

  // Generate JSON output
  $output           = new StdClass();
  $output->markers  = $markerManager->toArray();

  echo json_encode($output);*/

?>