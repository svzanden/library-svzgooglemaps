<?php

  session_start();

  // Including of some sample data
  //require_once('data.php');

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/generic/MarkerManager.php');

  $viewPort        = SVZ_Solutions_Generic_ViewPort::getInstance();

  $markerTypes101  = isset($_GET['marker-types-101']) ? $_GET['marker-types-101'] : '';
  $markerTypes102  = isset($_GET['marker-types-102']) ? $_GET['marker-types-102'] : '';
  $markerTypes103  = isset($_GET['marker-types-103']) ? $_GET['marker-types-103'] : '';
  $markerTypes104  = isset($_GET['marker-types-104']) ? $_GET['marker-types-104'] : '';
  $markerTypes105  = isset($_GET['marker-types-105']) ? $_GET['marker-types-105'] : '';
  $markerTypes106  = isset($_GET['marker-types-106']) ? $_GET['marker-types-106'] : '';
  $markerTypes107  = isset($_GET['marker-types-107']) ? $_GET['marker-types-107'] : '';
  $markerTypes108  = isset($_GET['marker-types-108']) ? $_GET['marker-types-108'] : '';
  $markerTypes109  = isset($_GET['marker-types-109']) ? $_GET['marker-types-109'] : '';
  $markerTypes110  = isset($_GET['marker-types-110']) ? $_GET['marker-types-110'] : '';
  if ($markerTypes101 == "") {$markerTypes101 = "0";}
  if ($markerTypes102 == "") {$markerTypes102 = "0";}
  if ($markerTypes103 == "") {$markerTypes103 = "0";}
  if ($markerTypes104 == "") {$markerTypes104 = "0";}
  if ($markerTypes105 == "") {$markerTypes105 = "0";}
  if ($markerTypes106 == "") {$markerTypes106 = "0";}
  if ($markerTypes107 == "") {$markerTypes107 = "0";}
  if ($markerTypes108 == "") {$markerTypes108 = "0";}
  if ($markerTypes109 == "") {$markerTypes109 = "0";}
  if ($markerTypes110 == "") {$markerTypes110 = "0";}

  //////filtering

  $mapClusterMode  = !empty($_SESSION['map_cluster_mode']) ? $_SESSION['map_cluster_mode'] : SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_DISTANCE;

  $markerManager    = new SVZ_Solutions_Generic_Marker_Manager();
  $markerManager->setListDataLoadUrl('projectlist.php');
  $markerManager->setClusterMode($mapClusterMode);
  //$markerManager->setMarkerTitleTemplate('%country%%, @street%%, @housenumber%', 8);
  //$markerManager->import($data);

  $serverName   = 'localhost';
  $username     = 'svzsolu_dev';
  $password     = 'svzsolu@d3v';
  $databaseName = 'svzsolu_dev_maps';
  $connection   = mysql_connect($serverName, $username, $password);

  if ($connection)
  {
    mysql_select_db($databaseName, $connection);

    $positionSql = ' WHERE ' . $viewPort->getSqlQueryPart() .
                   ' AND (camptype_id = ' . mysql_real_escape_string($markerTypes101) . '  OR  camptype_id = ' . mysql_real_escape_string($markerTypes102) . ' OR  camptype_id = ' . mysql_real_escape_string($markerTypes103) . ' OR  camptype_id = ' . mysql_real_escape_string($markerTypes103) . ' OR  camptype_id = ' . mysql_real_escape_string($markerTypes104) . ' OR  camptype_id = ' . mysql_real_escape_string($markerTypes105) . ' OR  camptype_id = ' . mysql_real_escape_string($markerTypes106) . ' OR  camptype_id = ' . mysql_real_escape_string($markerTypes107) . ' OR  camptype_id = ' . mysql_real_escape_string($markerTypes108) . ' OR  camptype_id = ' . mysql_real_escape_string($markerTypes109) . ' OR  camptype_id = ' . mysql_real_escape_string($markerTypes110) . '  ) ' .
                   ' AND (active = "1")';

    $query  = 'SELECT * FROM campdetails_tb ' . $positionSql;

    $result = mysql_query($query, $connection);

    if ($result)
    {
      while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
      {

        $title            = $row['name'];
        $id               = (int)$row['maindetails_id'];
        $geocodeLatitude  = (float)$row['lat'];
        $geocodeLongitude = (float)$row['lng'];
        $marker = new SVZ_Solutions_Generic_Marker($row['camptype_id']);
        //$marker           = new SvzGoogleMapsMarker('nameofmarkertype');
        //$marker->setDataLoadUrl('project.php?id=' . $row['id']);


        //$marker->setDataLoadUrl('project.php?id=' . $project['id'] . '&type=' . $project['type']);

        $marker->setDataLoadUrl('project.php?id=' . $row['parent_id']);

        $markerGeocode = new SVZ_Solutions_Generic_Geocode($geocodeLatitude, $geocodeLongitude);
        $marker->setGeocode($markerGeocode);
        $marker->setEntityId($id);
        $marker->setTitle($title);
        $markerManager->addMarker($marker);

      }
    }

    @mysql_close($connection);
  }


  // Generate JSON output
  $output           = new StdClass();
  $output->markers  = $markerManager->toArray();

  echo json_encode($output);

?>