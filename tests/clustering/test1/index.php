<?php

  /**
   * Copyright (c) 2009, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Advanced demo 2
   * @description:  This demo shows plotting all markers on a map and filtering on marker types.
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.2
   * @versionDate:  2010-02-07
   * @date:         2010-02-07
   */

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/maps/Map.php');

  $map                          = SVZ_Solutions_Maps_Map::getInstance(SVZ_Solutions_Maps_Map::MAP_TYPE_GOOGLE_MAPS, '3');
  $map->setWidth(900);
  $map->setHeight(600);

  // Sets the id of the container (HTMLDomElement) the map must be put on.
  $map->setContainerId('map');

  // Set the absolute or relative location of the file which will return your data like markers.
  $map->setDataLoadUrl('australia-projects.php');

  // Sets the default map type to roadmap.
  $map->setMapType('roadmap');

  // Sets the zoom level to start with to 8.
  $map->setZoomLevel(3);

  // Sets the geocode the map should start at centered.
  $map->setCenterGeocode(new SVZ_Solutions_Generic_Geocode(-19.957996, 119.771243));

  // Setting the cluster types (related to the current sample data)
  $types = array('101', '102', '103', '104', '105',
'106', '107', '108', '109', '110');

  // Create for each type a marker type
  foreach ($types as $type)
  {
    // Add a type marker type
    $markerType = new SVZ_Solutions_Generic_Marker_Type($type);
    $map->addMarkerType($markerType);
  }

    // Add a cluster marker type
  $markerType = new SVZ_Solutions_Generic_Marker_Type(SVZ_Solutions_Generic_Marker_Type::MARKER_TYPE_CLUSTER);
  $markerType->setClickAction('zoom');

  $map->addMarkerType($markerType);

  // Add a list marker type
  $markerType = new SVZ_Solutions_Generic_Marker_Type(SVZ_Solutions_Generic_Marker_Type::MARKER_TYPE_LIST);
  $map->addMarkerType($markerType);

?>
<!DOCTYPE HTML>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>SVZ Solutions - Maps - Advanced demo 2</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
  <link rel="stylesheet" type="text/css" href="../../../inc/css/svzmaps.css" media="all" />
  <link rel="stylesheet" type="text/css" href="../../../demos/inc/css/svzmaps-your.css" media="all" />

  <!--[if lte IE 9]>
  <link rel="stylesheet" type="text/css" href="../../inc/css/svzmaps-your-ie.css" media="all" />
  <![endif]-->

  <!-- BEGIN: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->
  <link rel="stylesheet" type="text/css" href="../../../demos/inc/css/demo.css" media="all" />
  <!-- END: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->

  <script type="text/javascript">
  // <![CDATA[
    var djConfig = {
    parseOnLoad: true,
    cacheBust: '<?php echo SVZ_Solutions_Maps_Map::APP_VERSION; ?>',
    baseUrl: "./",
    modulePaths: { svzsolutions: "../../../inc/svzsolutions", yourlib: "../../../inc/yourlib" }
    };
  // ]]>
  </script>
  <script src="http://ajax.googleapis.com/ajax/libs/dojo/1.5.0/dojo/dojo.xd.js"></script>
  <script type="text/javascript">
  // <![CDATA[

    /**
     * Function which is called after the DOM has finished loading
     *
     * @param void
     * @return void
     */
    var init = function()
    {
      var mapManager  = new svzsolutions.maps.MapManager();

      // The SVZ_Solutions_Maps_Google_Maps_Map php class will generate a config object depending on your settings for you,
      // this generated object can be encoded into a JSON string and can be put encoded into the svzsolutions.maps.MapManager object.
      var map         = mapManager.initByConfig('<?php echo json_encode($map->getConfig()); ?>');

      // Initialize your extensions
      var extension   = new yourlib.maps.googlemaps.ExtensionAdvancedDemo4(map);

      // Startup all the maps (call after subscribing within your extensions)
      mapManager.startup();
    };

    dojo.require('svzsolutions.layer');
    dojo.require('yourlib.maps.googlemaps.ExtensionAdvancedDemo4');
    dojo.addOnLoad(init);
  // ]]>
  </script>
</head>
<body>

  <div class="demo-information">
    <h1>Clustering test 2: Plotting all markers on a map and filtering on marker types</h1>
    <p><a href="../../index.html">&#171; back to demo overview</a></p>
    <p>This demo shows plotting all markers on a map and filtering on marker types.<br />
    More info about this can be found <a href="#">here *TODO*</a>.</p>
  </div>

  <div class="sg-filter-holder">
    <form id="sg-filter" method="post">
      <p>
        <label for="marker-type-type1">Type 1:</label><span class="sg-marker sg-marker-type1"></span><input class="input-marker-type" type="checkbox" id="marker-type-type1" name="marker-types[]" value="type1" checked="checked" /><br /><br />
        <label for="marker-type-type2">Type 2:</label><span class="sg-marker sg-marker-type2"></span><input class="input-marker-type" type="checkbox" id="marker-type-type2" name="marker-types[]" value="type2" checked="checked" /><br /><br />
        <label for="marker-type-type3">Type 3:</label><span class="sg-marker sg-marker-type3"></span><input class="input-marker-type" type="checkbox" id="marker-type-type3" name="marker-types[]" value="type3" checked="checked" /><br /><br />
        <label for="marker-type-type4">Type 4:</label><span class="sg-marker sg-marker-type4"></span><input class="input-marker-type" type="checkbox" id="marker-type-type4" name="marker-types[]" value="type4" checked="checked" /><br /><br />
        <label for="marker-type-type5">Type 5:</label><span class="sg-marker sg-marker-type5"></span><input class="input-marker-type" type="checkbox" id="marker-type-type5" name="marker-types[]" value="type5" checked="checked" /><br /><br />
        <span class="sg-marker sg-marker-101"></span><label for="marker-type-101">Caravan Park</label><input class="input-marker-type" type="checkbox" id="marker-type-101" name="marker-types-101" value="101" checked="checked" /><br /><br />
        <span class="sg-marker sg-marker-102"></span><label for="marker-type-102">Rest Stop</label><input class="input-marker-type" type="checkbox" id="marker-type-102" name="marker-types-102" value="102" checked="checked" /><br /><br />
        <span class="sg-marker sg-marker-103"></span><label for="marker-type-103">National Park or State Forest</label><input class="input-marker-type" type="checkbox" id="marker-type-103" name="marker-types-103" value="103" checked="checked" /><br /><br />
        <span class="sg-marker sg-marker-105"></span><label for="marker-type-105">Council Reserve or Park</label><input class="input-marker-type" type="checkbox" id="marker-type-105" name="marker-types-105" value="105" checked="checked" /><br /><br />
        <span class="sg-marker sg-marker-106"></span><label for="marker-type-106">Showground</label><input class="input-marker-type" type="checkbox" id="marker-type-106" name="marker-types-106" value="106" checked="checked" /><br /><br />
        <span class="sg-marker sg-marker-107"></span><label for="marker-type-107">RV Friendly Town</label><input class="input-marker-type" type="checkbox" id="marker-type-107" name="marker-types-107" value="107" checked="checked" /><br /><br />
        <span class="sg-marker sg-marker-109"></span><label for="marker-type-109">Tourist Info Center</label><input class="input-marker-type" type="checkbox" id="marker-type-109" name="marker-types-109" value="109" checked="checked" /><br /><br />
        <span class="sg-marker sg-marker-110"></span><label for="marker-type-110">Dump Point</label><input class="input-marker-type" type="checkbox" id="marker-type-110" name="marker-types-110" value="110" checked="checked" /><br /><br />
        <span class="sg-marker sg-marker-104"></span><label for="marker-type-104">No Camping</label><input class="input-marker-type" type="checkbox" id="marker-type-104" name="marker-types-104" value="104" checked="checked" /><br /><br />

        <br /><br />
      </p>
    </form>
  </div>

  <div id="<?php echo $map->getContainerId(); ?>" class="map-holder" style="width: <?php echo $map->getWidth(); ?>px; height: <?php echo $map->getHeight(); ?>px"></div>

</body>
</html>