<?php

  require_once(dirname(__FILE__) . '/../../../includes/svzsolutions/maps/googlemaps/Geocode.php');

  $address = new SVZ_Solutions_Generic_Address();
  $address->setCountry('Spain');
  $address->setCity('Jalon');
  //$address->setStreet('Lammertkamp');
  //$address->setHouseNumber(40);

  $geocoding = new SVZ_Solutions_Maps_Google_Maps_Geocode();
  $geocoding->setAddress($address);

  $result = $geocoding->retrieve();

  echo '<pre>';

  if ($result->hasPlaceMarks())
  {
    foreach ($result->getPlaceMarks() as $placeMark)
    {
      print_r($placeMark);
      echo '<br />';
    }
  }

  echo '</pre>';

  echo '<br />=============================================<br />';

  $address = new SVZ_Solutions_Generic_Address();
  $address->setCountry('Nederland');
  $address->setCity('Harderwijk');
  $address->setStreet('Lammertkamp');
  $address->setHouseNumber(40);

  $geocoding = new SVZ_Solutions_Maps_Google_Maps_Geocode();
  $geocoding->setAddress($address);

  $result = $geocoding->retrieve();

  echo '<pre>';

  if ($result->hasPlaceMarks())
  {
    foreach ($result->getPlaceMarks() as $placeMark)
    {
      print_r($placeMark);
      echo '<br />';
    }
  }

  echo '</pre>';

  echo '<br />=============================================<br />';

  $address = new SVZ_Solutions_Generic_Address();
  $address->setCountry('Nederland');
  $address->setCity('Ha');

  $geocoding = new SVZ_Solutions_Maps_Google_Maps_Geocode();
  $geocoding->setAddress($address);

  $result = $geocoding->retrieve();

  echo '<pre>';

  if ($result->hasPlaceMarks())
  {
    foreach ($result->getPlaceMarks() as $placeMark)
    {
      print_r($placeMark);
      echo '<br />';
    }
  }

  echo '</pre>';

  echo '<br />=============================================<br />';

  // Batched geocoding sample
  $geocodes   = array();
  $geocodes[] = array('lat' => 52.386387, 'lng' => 5.288031);
  $geocodes[] = array('lat' => 52.347968, 'lng' => 52.347968);

  $geocoding = new SVZ_Solutions_Maps_Google_Maps_Geocode();

  foreach ($geocodes as $geocode)
  {
    $geocode = new SVZ_Solutions_Generic_Geocode($geocode['lat'], $geocode['lng']);

    $geocoding->setGeocode($geocode);

    $result = $geocoding->retrieve();

    echo '<pre>';

    if ($result->hasPlaceMarks())
    {
      foreach ($result->getPlaceMarks() as $placeMark)
      {
        $address = $placeMark->getAddress();

        print_r($address);

        // In this case we only need the first most detailed placemark
        break;
      }
    }

    flush(); // Flush the results to the browser so you see the feedback when it is available

    usleep(1000000); // Wait for 1 second until doing another request (to prevent spamming of Google)

    echo '</pre>';

  }

  echo '<br />=============================================<br />';

  $geocode = new SVZ_Solutions_Generic_Geocode(52.347968, 52.347968);

  $geocoding = new SVZ_Solutions_Maps_Google_Maps_Geocode();
  $geocoding->setGeocode($geocode);

  $result = $geocoding->retrieve();

  echo '<pre>';

  if ($result->hasPlaceMarks())
  {
    foreach ($result->getPlaceMarks() as $placeMark)
    {
      print_r($placeMark);
      echo '<br />';
    }
  }

  echo '</pre>';



?>