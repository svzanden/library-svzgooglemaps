<?php

  /**
   * Copyright (c) 2009, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions ViewPort test 1
   * @description:  This test places a marker on each of the 5 bounds of the viewport,
   *                will test calculation of the NorthWest and SouthEast points
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.6
   * @versionDate:  2010-09-25
   * @date:         2010-09-25
   */

  // Including of some sample data
  require_once('../../../demos/testdata/data.php');

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/generic/MarkerManager.php');

  $viewPort = SVZ_Solutions_Generic_ViewPort::getInstance();

  // Reading out data applied in the requests
  $mapClusterMode   = SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_NONE;

  $markerManager    = new SVZ_Solutions_Generic_Marker_Manager();

  $southWestMarker  = new SVZ_Solutions_Generic_Marker('BBvk', $viewPort->getBounds()->getGeocodeSouthWest()->getLatitude(), $viewPort->getBounds()->getGeocodeSouthWest()->getLongitude());
  $markerManager->addMarker($southWestMarker);

  $southEastMarker  = new SVZ_Solutions_Generic_Marker('BBvk', $viewPort->getBounds()->getGeocodeSouthEast()->getLatitude(), $viewPort->getBounds()->getGeocodeSouthEast()->getLongitude());
  $markerManager->addMarker($southEastMarker);

  $northWestMarker  = new SVZ_Solutions_Generic_Marker('NBvk', $viewPort->getBounds()->getGeocodeNorthWest()->getLatitude(), $viewPort->getBounds()->getGeocodeNorthWest()->getLongitude());
  $markerManager->addMarker($northWestMarker);

  $northEastMarker  = new SVZ_Solutions_Generic_Marker('Type1', $viewPort->getBounds()->getGeocodeNorthEast()->getLatitude(), $viewPort->getBounds()->getGeocodeNorthEast()->getLongitude());
  $markerManager->addMarker($northEastMarker);

  $centerMarker  = new SVZ_Solutions_Generic_Marker('Type2', $viewPort->getBounds()->getGeocodeCenter()->getLatitude(), $viewPort->getBounds()->getGeocodeCenter()->getLongitude());
  $markerManager->addMarker($centerMarker);

  //$markerManager->setListDataLoadUrl('data-info-window-list.php');
  $markerManager->setClusterMode($mapClusterMode);
  //$markerManager->import($data);

  // Generate JSON output
  $output           = new StdClass();
  $output->markers  = $markerManager->toArray();

  echo json_encode($output);

?>