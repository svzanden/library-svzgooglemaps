/Library/Frameworks/GDAL.framework/Versions/1.7/unix/bin/gdalinfo floorplan.jpg
Driver: JPEG/JPEG JFIF
Files: floorplan.jpg
Size is 782, 551
Coordinate System is `'
Image Structure Metadata:
  SOURCE_COLOR_SPACE=YCbCr
  INTERLEAVE=PIXEL
  COMPRESSION=JPEG
Corner Coordinates:
Upper Left  (    0.0,    0.0)
Lower Left  (    0.0,  551.0)
Upper Right (  782.0,    0.0)
Lower Right (  782.0,  551.0)
Center      (  391.0,  275.5)
Band 1 Block=782x1 Type=Byte, ColorInterp=Red
  Image Structure Metadata:
    COMPRESSION=JPEG
Band 2 Block=782x1 Type=Byte, ColorInterp=Green
  Image Structure Metadata:
    COMPRESSION=JPEG
Band 3 Block=782x1 Type=Byte, ColorInterp=Blue
  Image Structure Metadata:
    COMPRESSION=JPEG

    
// Replace the size vars in the following command
/Library/Frameworks/GDAL.framework/Versions/1.7/unix/bin/gdal_translate -of VRT -a_srs EPSG:4326 -gcp 0 0 -180 90 -gcp 782 0 180 90 -gcp 782 551 180 -90 floorplan.jpg floorplan.vrt

/Library/Frameworks/GDAL.framework/Versions/1.7/unix/bin/gdalwarp -of VRT -t_srs EPSG:4326 floorplan.vrt floorplan2.vrt

/Library/Frameworks/GDAL.framework/Versions/1.7/unix/bin/gdal2tiles.py -p raster -k floorplan2.vrt -w google -z 0-4


ABQIAAAAoce2A2zKom_SqE4opC0YIxRXHH98t8VblHjT9ZucfDq_kAQYSRQG8oR6IQbKWwNAiYwDzzi096OBiQ





/Library/Frameworks/GDAL.framework/Versions/1.7/unix/bin/gdalinfo floorplan2.png
Driver: JPEG/JPEG JFIF
Files: floorplan2.jpg
Size is 1000, 1000
Coordinate System is `'
Image Structure Metadata:
  SOURCE_COLOR_SPACE=YCbCr
  INTERLEAVE=PIXEL
  COMPRESSION=JPEG
Corner Coordinates:
Upper Left  (    0.0,    0.0)
Lower Left  (    0.0, 1000.0)
Upper Right ( 1000.0,    0.0)
Lower Right ( 1000.0, 1000.0)
Center      (  500.0,  500.0)
Band 1 Block=1000x1 Type=Byte, ColorInterp=Red
  Image Structure Metadata:
    COMPRESSION=JPEG
Band 2 Block=1000x1 Type=Byte, ColorInterp=Green
  Image Structure Metadata:
    COMPRESSION=JPEG
Band 3 Block=1000x1 Type=Byte, ColorInterp=Blue
  Image Structure Metadata:
    COMPRESSION=JPEG

/Library/Frameworks/GDAL.framework/Versions/1.7/unix/bin/gdal_translate -of VRT -a_srs EPSG:4326 -gcp 0 0 -180 90 -gcp 1000 0 180 90 -gcp 1000 1000 180 -90 floorplan2.png floorplan3.vrt

/Library/Frameworks/GDAL.framework/Versions/1.7/unix/bin/gdalwarp -of VRT -t_srs EPSG:4326 floorplan3.vrt floorplan4.vrt

/Library/Frameworks/GDAL.framework/Versions/1.7/unix/bin/gdal2tiles.py -p raster -k floorplan3.vrt -w google -z 0-4