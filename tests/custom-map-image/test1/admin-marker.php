<?php 

	// Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/generic/InfoWindowContent.php');

  // Get parameters from URL
  $projectId    = isset($_GET['id']) ? $_GET['id'] : 0;

  $output = array();

  $output['content'] = '<div class="sg-message-holder sg-error" style="display: none;"><p class="message"></p></div>';
  
  $output['content'] .= '<a class="marker-remove" href="#">&raquo; Verwijder deze positie.</a><br />' .
  									'<a class="marker-edit" href="#">&raquo; Wijzig details van deze positie.</a>';
  
  $output['content'] = '<div>' . $output['content'] . '</div>';

  echo json_encode($output);

?>