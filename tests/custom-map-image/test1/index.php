<?php

  /**
   * Copyright (c) 2010, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Custom map image test 1
   * @description:  This demo shows the use of a custom map image.
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.6.2
   * @versionDate:  2010-11-07
   * @date:         2010-11-07
   */

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/maps/Map.php');

  // Add the registered admin markers
  $registeredMarkers   = array();
  $registeredMarkers[] = array('latitude' => 25.159669026261486, 'longitude' => 42.749890999999984, 'entityId' => 101);
  $registeredMarkers[] = array('latitude' => -35.46562321114592, 'longitude' => -94.7110465, 'entityId' => 102);

  // BEGIN ADMIN PART
  // Create a new instance of Google Maps version 3
  $map                          = SVZ_Solutions_Maps_Map::getInstance(SVZ_Solutions_Maps_Map::MAP_TYPE_GOOGLE_MAPS, '3');
  $map->setWidth(600);
  $map->setHeight(400);

  // Sets the id of the container (HTMLDomElement) the map must be put on.
  $map->setContainerId('map');

  // Sets the default map type to satellite
  $map->setMapType('roadmap');

  // Enable the street view control
  $map->disableStreetViewControl();

  // Disable the scale control
  $map->disableScaleControl();

  // Disable the Map type control
  $map->disableMapTypeControl();

  // Sets the zoom level to start with to 8.
  //$map->setZoomLevel(18);
  $map->setZoomLevel(2);

  // Sets the geocode the map should start at centered.
  //$map->setCenterGeocode(new SVZ_Solutions_Generic_Geocode(51.5479672, 5.648499));
  //$map->setCenterGeocode(new SVZ_Solutions_Generic_Geocode(52.618898, 4.751187));
  $map->setCenterGeocode(new SVZ_Solutions_Generic_Geocode(-2.1149759615819046, -2.250109000000018));

  // Add a admin marker type
  $markerType = new SVZ_Solutions_Generic_Marker_Type('admin');
  $markerType->enableIcon();
  $markerType->getLayer()->setName('admin');
  $markerType->getLayer()->setTypeFixed();
  $map->addMarkerType($markerType);

  foreach ($registeredMarkers as $registeredMarker)
  {
    $marker     = new SVZ_Solutions_Generic_Marker('admin', $registeredMarker['latitude'], $registeredMarker['longitude']);
    $marker->setDraggable(true);
    $marker->setEntityId($registeredMarker['entityId']);
    $marker->setDataLoadUrl('admin-marker.php?entityId=' . $registeredMarker['entityId']);
    $map->addMarker($marker);
  }
  // END ADMIN PART

  // BEGIN LIVE PART
  // Create a new instance of Google Maps version 3
  $map2                          = SVZ_Solutions_Maps_Map::getInstance(SVZ_Solutions_Maps_Map::MAP_TYPE_GOOGLE_MAPS, '3');
  $map2->setWidth(600);
  $map2->setHeight(400);

  // Sets the id of the container (HTMLDomElement) the map must be put on.
  $map2->setContainerId('map2');

  // Sets the default map type to satellite
  $map2->setMapType('roadmap');

  // Disable the street view control
  $map2->disableStreetViewControl();

  // Disable the scale control
  $map2->disableScaleControl();

  // Disable the Map type control
  $map2->disableMapTypeControl();

  // Sets the zoom level to start with to 8.
  //$map->setZoomLevel(18);
  $map2->setZoomLevel(2);

  // Sets the geocode the map should start at centered.
  //$map->setCenterGeocode(new SVZ_Solutions_Generic_Geocode(51.5479672, 5.648499));
  //$map->setCenterGeocode(new SVZ_Solutions_Generic_Geocode(52.618898, 4.751187));
  $map2->setCenterGeocode(new SVZ_Solutions_Generic_Geocode(-2.1149759615819046, -2.250109000000018));

  // Add a admin marker type
  $markerType = new SVZ_Solutions_Generic_Marker_Type('spot');
  $markerType->enableDataLoadOnMouseOver();
  //$markerType->enableIcon();
  //$markerType->getLayer()->setName('admin');
  //$markerType->getLayer()->setTypeFixed();
  $map2->addMarkerType($markerType);

  foreach ($registeredMarkers as $registeredMarker)
  {
    $marker     = new SVZ_Solutions_Generic_Marker('spot', $registeredMarker['latitude'], $registeredMarker['longitude']);
    //$marker->setDraggable(true);
    $marker->setContent('Drag me!');    
    $marker->setEntityId($registeredMarker['entityId']);
    $marker->setDataLoadUrl('marker-info.php?entityId=' . $registeredMarker['entityId']);
    $map2->addMarker($marker);
  }
  // END LIVE PART

?>
<!DOCTYPE HTML>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>SVZ Solutions - Maps - Custom map image test 1</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
  <link rel="stylesheet" type="text/css" href="../../../inc/css/svzmaps.css" media="all" />
  <link rel="stylesheet" type="text/css" href="../../../demos/inc/css/svzmaps-your.css" media="all" />

  <!--[if lte IE 9]>
  <link rel="stylesheet" type="text/css" href="../../inc/css/svzmaps-your-ie.css" media="all" />
  <![endif]-->

  <!-- BEGIN: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->
  <link rel="stylesheet" type="text/css" href="../../../demos/inc/css/demo.css" media="all" />
  <!-- END: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->

  <script type="text/javascript">
  // <![CDATA[
    var djConfig = {
    parseOnLoad: false,
    cacheBust: '<?php echo SVZ_Solutions_Maps_Map::APP_VERSION; ?>',
    baseUrl: "./",
    modulePaths: { svzsolutions: "../../../inc/svzsolutions", tests: "../../inc/tests" }
    };
  // ]]>
  </script>
  <script src="http://ajax.googleapis.com/ajax/libs/dojo/1.5.0/dojo/dojo.xd.js"></script>
  <script type="text/javascript">
  // <![CDATA[

    /**
     * Function which is called after the DOM has finished loading
     *
     * @param void
     * @return void
     */
    var init = function()
    {
      var mapManager  = new svzsolutions.maps.MapManager();

      // The SVZ_Solutions_Maps_Google_Maps_Map php class will generate a config object depending on your settings for you,
      // this generated object can be encoded into a JSON string and can be put encoded into the svzsolutions.maps.MapManager object.
      var map         = mapManager.initByConfig('<?php echo json_encode($map->getConfig()); ?>');
      var map2        = mapManager.initByConfig('<?php echo json_encode($map2->getConfig()); ?>');

   		// Initialize your extensions
   		var extension1Config = {
					tileUrl: 'http://development.svzsolutions.nl/opensource/svzmaps/tests/custom-map-image/test1/floorplan3/'
			}   		
   		
      var extension1  = new tests.maps.googlemaps.ExtensionCustomMapImageTest1Map(map, extension1Config);

			var extensions2Config = { urls: {
			  	removeMarker: 'admin-remove-marker.php',
			  	storeMarker: 'admin-store-markers.php',
			  	storeCenterZoom: 'admin-store-center-zoom.php'
				} };
      
      var extension2  = new tests.maps.googlemaps.ExtensionCustomMapImageTest1(map, extensions2Config);

			var extension3Config = {
					tileUrl: 'http://development.svzsolutions.nl/opensource/svzmaps/tests/custom-map-image/test1/floorplan3/'
			}
      
      var extension3  = new tests.maps.googlemaps.ExtensionCustomMapImageTest1Map(map2, extension3Config);

      // Startup all the maps (call after subscribing within your extensions)
      mapManager.startup();
    };

    dojo.require('svzsolutions.all');
    dojo.require('tests.maps.googlemaps.ExtensionCustomMapImageTest1');
    dojo.require('tests.maps.googlemaps.ExtensionCustomMapImageTest1Map');
    dojo.addOnLoad(init);
  // ]]>
  </script>
</head>
<style type="text/css">
	.sg-marker-spot {
	border: 1px solid red;
	}

		.sg-marker-spot:hover {
		background-image: url('http://labs.google.com/ridefinder/images/mm_20_red.png');
		}
</style>
<body>

  <div class="demo-information">
    <h1>Custom map image test 1: Usage of the custom map images</h1>
    <p><a href="../../index.html">&#171; back to demo overview</a></p>
    <p>This demo shows the use of the custom map images.<br />
    More info on these controls can be found <a href="#">here *TODO*</a></p>
  </div>

	<div class="demo-settings">
		<a id="add-marker-button" href="#">Add a spot</a><br />
		<a id="store-marker-button" href="#">Store current spots</a><br />
		<a id="store-center-zoom-button" href="#">Store current center and zoom level as default position of the live map</a><br />		
	</div>
	<div class="message-holder" style="display: none"><div class="message"></div></div>
	<div style="float: left; width: 50%;">
  	<h3 style="margin: 10px;">Admin map</h3>
    <div id="<?php echo $map->getContainerId(); ?>" class="map-holder" style="width: <?php echo $map->getWidth(); ?>px; height: <?php echo $map->getHeight(); ?>px"></div>
  	<div id="information"></div>
  </div>

  <div style="float: left; width: 50%;">
  	<h3 style="margin: 10px;">Sample live map</h3>
  	<div id="<?php echo $map2->getContainerId(); ?>" class="map-holder" style="width: <?php echo $map2->getWidth(); ?>px; height: <?php echo $map2->getHeight(); ?>px"></div>
  	<div id="information-live"></div>
  </div>

</body>
</html>