<?php 

	$zoomLevel 				= (!empty($_POST['zoomLevel']) ? (int)$_POST['zoomLevel'] : 0);
	$geocodeLatitude 	= (!empty($_POST['geocodeLatitude']) ? (float)$_POST['geocodeLatitude'] : 0);
	$geocodeLongitude = (!empty($_POST['geocodeLongitude']) ? (float)$_POST['geocodeLongitude'] : 0);
	
	//echo '[' . $zoomLevel . '][' . $geocodeLatitude . '][' . $geocodeLongitude . ']';
	
	$response 								= array();	
	
	$response['messageType'] 	= 'success';
	$response['message'] 			= 'De center en zoom level gegevens zijn successvol opgeslagen.';
	//$response['messageType'] 	= 'error';
	//$response['message'] 			= 'Er ging iets fout bij het opslaan van de center en zoom level gegevens.';
	
	echo json_encode($response);

?>