<?php 

	// Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/generic/InfoWindowContent.php');

  // Get parameters from URL
  $entityId    = isset($_GET['entityId']) ? (int)$_GET['entityId'] : 0;

  $output = array();

  $output['content'] = '<div class="sg-message-holder sg-error" style="display: none;"><p class="message"></p></div>';
  
  $output['content'] .= 'Informatie over deze marker met id [' . $entityId . ']';
  
  $output['content'] = '<div>' . $output['content'] . '</div>';

  echo json_encode($output);

?>