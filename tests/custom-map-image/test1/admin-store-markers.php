<?php 

	// Read out the variables send through POST
	$numMarkersSend = count($_POST);
	
	for ($i = 0; $i < $numMarkersSend; $i++)
	{
		$marker = explode('|', $_POST[$i]);
		
		$geocodeLatitude 	= $marker[0];
		$geocodeLongitude = $marker[1];
		$entityId 				= $marker[2];		
		
		if (!empty($entityId))
		{
			// It's a change action so change the stuff in the db with this id
			//echo 'Edit!<br />';
		}
		else 
		{
			// Add a new marker
			//echo 'Add!<br />';
		}
	}

	$response 								= array();	
	
	$response['messageType'] 	= 'success';
	$response['message'] 			= 'De marker gegevens zijn successvol opgeslagen.';
	//$response['messageType'] 	= 'error';
	//$response['message'] 			= 'Er ging iets fout bij het opslaan van deze markers.';
	
	echo json_encode($response);
	
?>