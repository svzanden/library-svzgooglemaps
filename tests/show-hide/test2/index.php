<?php

  /**
   * Copyright (c) 2010, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Basic demo 2
   * @description:  Test to render a map when the div is hidden and then show it
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.6
   * @versionDate:  2010-01-26
   * @date:         2010-01-26
   */

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/maps/Map.php');

  // Create a new instance of Google Maps version 3
  $map                          = SVZ_Solutions_Maps_Map::getInstance(SVZ_Solutions_Maps_Map::MAP_TYPE_GOOGLE_MAPS, '3');
  $map->setWidth(900);
  $map->setHeight(600);

  // Sets the id of the container (HTMLDomElement) the map must be put on.
  $map->setContainerId('map');

  // Sets the default map type to satellite
  $map->setMapType('satellite');

  // Sets the type control to a dropdown and places it in the middle top left corner.
  $map->setMapTypeControlStyle('dropdown_menu');
  $map->setMapTypeControlPosition('top_left');

  // Sets the scale control and places it in the middle at the bottom
  $map->setMapScaleControlStyle('default');
  $map->setMapScaleControlPosition('bottom');

  // Sets the navigations control style to the one on android and place it in the top right corner.
  $map->setMapNavigationControlStyle('android');
  $map->setMapNavigationControlPosition('top_right');

  // Sets the zoom level to start with to 8.
  $map->setZoomLevel(8);

  // Sets the geocode the map should start at centered.
  $map->setCenterGeocode(new SVZ_Solutions_Generic_Geocode(51.5479672, 5.648499));

  // Add a admin marker type
  $markerType = new SVZ_Solutions_Generic_Marker_Type('admin');
  $markerType->enableIcon();
  $markerType->getLayer()->setName('admin');
  $markerType->getLayer()->setTypeFixed();
  $map->addMarkerType($markerType);

  // Add a single admin marker
  $marker     = new SVZ_Solutions_Generic_Marker('admin', 51.5479672, 5.648499);
  $marker->setDraggable(true);
  $marker->setContent('Drag me!');
  $map->addMarker($marker);

?>
<!DOCTYPE HTML>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>SVZ Solutions - Maps - Basic demo 2</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
  <link rel="stylesheet" type="text/css" href="../../../inc/css/svzmaps.css" media="all" />
  <link rel="stylesheet" type="text/css" href="../../inc/css/svzmaps-your.css" media="all" />

  <!--[if lte IE 9]>
  <link rel="stylesheet" type="text/css" href="../../inc/css/svzmaps-your-ie.css" media="all" />
  <![endif]-->

  <!-- BEGIN: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->
  <link rel="stylesheet" type="text/css" href="../../inc/css/demo.css" media="all" />
  <!-- END: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->

  <script type="text/javascript">
  // <![CDATA[
    var djConfig = {
    parseOnLoad: false,
    cacheBust: '<?php echo SVZ_Solutions_Maps_Map::APP_VERSION; ?>',
    baseUrl: "./",
    modulePaths: { svzsolutions: "../../../inc/svzsolutions" }
    };
  // ]]>
  </script>
  <script src="http://ajax.googleapis.com/ajax/libs/dojo/1.5.0/dojo/dojo.xd.js"></script>
  <script type="text/javascript">
  // <![CDATA[

    var map = null;

    /**
     * Function which is called after the DOM has finished loading
     *
     * @param void
     * @return void
     */
    var init = function()
    {
      var mapManager  = new svzsolutions.maps.MapManager({debugMode: true});

      // The SVZ_Solutions_Maps_Google_Maps_Map php class will generate a config object depending on your settings for you,
      // this generated object can be encoded into a JSON string and can be put encoded into the svzsolutions.maps.MapManager object.
      map             = mapManager.initByConfig('<?php echo json_encode($map->getConfig()); ?>');

      // Startup all the maps (call after subscribing within your extensions)
      mapManager.startup();
    };

    dojo.require('svzsolutions.layer');
    dojo.addOnLoad(init);
  // ]]>
  </script>
</head>
<body>

  <div class="demo-information">
    <h1>Show hide test 1: </h1>
  </div>

  <a href="#" onclick="var test = dojo.byId('<?php echo $map->getContainerId(); ?>'); if (test.style.display == 'block') { test.style.display = 'none'; } else { test.style.display = 'block'; } /*map.resize();*/return false;">Toggle</a>

  <div id="test" style="display:block;width:400px;height:300px;">
    <div id="<?php echo $map->getContainerId(); ?>" class="map-holder" style="width: <?php echo $map->getWidth(); ?>px; height: <?php echo $map->getHeight(); ?>px"></div>
  </div>

</body>
</html>