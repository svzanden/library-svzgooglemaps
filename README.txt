Copyright (c) 2010-2019, SVZ Solutions All Rights Reserved.
Available via BSD license, see license file included for details.

=== DEPENDENCIES ===
- Dojo Base or Full library v1.13.0
- PHP 5.6 or higher

=== RELEASE NOTES ===

v0.7.6 - 2019-09-03
- Added: Ability to register a different API key for the JS and for the PHP using the key SVZ_GOOGLE_MAPS_REST_API_KEY;
- Fixed: Geocoding service now uses the key instead of apiKey in the request;

v0.7.5 - 2018-05-18
- Changed: Some calls to http changed to https;

v0.7.4 - 2018-03-06
- Added: Bootstrap in some pages;
- Added: Switch in map to load the current stable release or the experimental release of Google Maps
- Fixed: On experimental release the Google Maps onidle event would trigger a loop in combination with the resize functions;
- Changed: Deprecated disableScrollWheel / disableDragOnTouch
- Changed: Cooperative gestureHandling is used on maps for better navigation triggers on desktop and mobile devices;
- Added: Method to set the gestueehandling used (default to cooperative) 

v0.7.3 - 2018-02-15
- Added: Support for a Google Maps API Key for the Geocode / Address Search API and the static maps

v0.7.2 - 2017-05-10
- Added: Support for a Google Maps API Key by defining constant SVZ_GOOGLE_MAPS_API_KEY;

v0.7.1 - 2016-10-30
- Fixed: Don't recenter the map after loading of data;

v0.7.0 - 2016-02-21
- Changed: Now loading the google maps library including the places api if needed;
- Added: Method to disable the scrollwheel;
- Removed: sensor = false setting (deprecated);
- Added: support for vw / vh width / height units;
- Changed: Upgraded to dojo 1.10.4;
- Added: Option to disable dragging of the map on touch devices;

v0.6.2 - 2013-03-22
- Changed: Improved performance rendering alot of markers;
- Changed: Demo's now use the AMD format;
- Fixed: getInstance of map not being a proper static function;
- Changed: infoWindow is no longer closed when the map is dragged;
- Changed: When the setDataLoadUrl is called it will reset the markers load data once flag;
- Added: Hook for when markers are being shown called "onMarkersShown";
- Added: Method to change the dataLoadUrl on the fly of the Map object;
- Changed: initByConfig now accepts an config object or a config json string;
- Added: Ability to provide the containerElem to place the map on;
- Changed: Now using the AMD format to require modules;
- Added: Api documentation of the JS library;
- Changed: Improved code documentation;
- Added: Hooks to capture the map type changed / mouse richt click / mouse double click and map being dragged;
- Fixed: When a developer wants to abort a refresh the map is no longer destroyed before an actually refresh;
- Changed: Events are now cleaned up before destroying everything;
- Added: Method to resposition a maps center (deprecates the direct use of the Google Maps object in the demo's);
- Added: New methods to retrieve the current users location in the Geocode class;
- Added: Moderate Demo 6 using the users location to set the center of a map;
- Added: Method in the MarkerManager to return the count per type on a cluster marker using enableReturnCountPerMarkerType;
- Added: Advanced Demo 8 showing usage of multiple maps combined with multiple extensions; 
- Added: Method to change the current zoom level of the map; 
- Added: Ability to close an infowindow through code;
- Added: Config option with the marker type to load data on mouse hover;
- Added: Hook to capture when a marker is mouse entered;
- Added: Hook to capture when a marker is clicked;
- Added: Method setShowInfoWindow so you can choose if a info window should be automaticly opened on first load
- Added: The event onInfoWindowContentLoaded is now also fired when the content is set without a Ajax call;
- Added: Support for the new positions and control seperation in Google Maps v3.3.6;
- Removed: Support for the navigation control;
- Changed: Redone the entire way controls are configured, now with more easy functions to set it;
- Added: Static method getAvailableZoomLevels to return all the possible zoom levels;
- Added: Static method getAvailableClusterModes to return all the possible cluster modes from the MarkerManager;
- Added: Now possible to give a map an ID to identify it later on in extensions;
- Added: Test for auto rotating a streetview image;
- Changed: Now using r.js as the build system;

v0.6.1 - 2010-10-03
- Fixed: After calling the resize function the map object it will also correct the center position;
- Fixed: Center positioning over a bunch of markers was only centered over the y-axis;
- Changed: Cleaned up a the demo's and changed parts of the test data to span other parts of the world;
- Fixed: Smartnavigation zoom level now also takes in consideration a wider width then height;
- Added: Demo 7 displaying the usage of polygons and polylines and klm files;

v0.6 - 2010-09-25

- Added: How do I page;
- Added: Math class in php and js which contains several calculation functions;
- Changed: Doctype from XHTML to HTML5;
- Added: Function to disable list markers being used;
- Changed: Upgraded include of dojo 1.4 library to the dojo 1.5.0 library;
- Added: Basic demo 4 showing how to enable Streetview;
- Added: Support for Streetview;
- Added: Advanced demo 6 showing the use of calculating routes between markers;
- Added: Demo's containing input for a geocode will now have this value validated;
- Added: Compiled / compressed version of svzmaps;
- Added: Resize function to the main map object to prevent grey tiles when changing the size of the map or show it after it's been rendered;

===================

v0.5 - 2010-04-25

- Added: Dojo demo showing the TabContainer and Contentpane loading a map through Ajax;
- Added: Class for storing bounds; 
- Added: Smart navigation for cluster markers; 
- Changed: Cluster markers are now centered within the bounderies all the markers within that cluster define;
- Changed: ViewPort is now a singleton class callable within the library;
- Changed: Extended advanced demo 1 with the ability to load a text through Ajax;

===================

v0.4 - 2010-04-05

- Changed: icon / shadow url for the default marker no longer need to be provided;
- Fixed: issue where the shadow would not be positioned right on the default marker;
- Added: advanced demo4 combining clustering with administrating a single admin marker;
- Added: basic demo 3 displaying wikipedia layers when available in v3;
- Added: support for the wikipedia layers, if they become available in the v3;
- Added: center latitude / longitude to load data requests;
- Fixed: issue when google maps was already loaded and creating a new instance of the mapManager object;
- Changed: further improved the geocoding webservice code;
- Changed: added ability to find an address or geocode location using the Geocoder;
- Added: moderate demo 4 which shows a administrating sample combined with address searching;
- Added: moderate demo 5 which shows a administrating sample combined with address searching and the ability to set the zoom levels, 
         all without a marker and based on the center of the map;
- Changed: upgraded from Geocoding Web Service v2 to v3;
- Changed: all demo's now use a CDN version of Dojo;
- Changed: moved demo + library js outside of the dojotoolkit folder;
- Added: support for multi layered markers;
- Changed: placed underlay of loader behind the main controls;
- Added: way to define custom markers in the static api;
- Removed: extensions are no longer loaded through the php library and no longer being eval'd;

===================

v0.3 - 2010-02-14
Third private release to a small group of testers

- Changed: implemented better way of loading multiple custom extensions;
- Removed: extensive usage of dojo.eval to improve performance;
- Added: viewPort class to generate some usefull query parts;
- Added: cleanup of the map on destroying the page;
- Added: functionality to cleanup a map using the MapManager;
- Added: test for cleanup of resources after closing the map;

===================

v0.2 - 2010-02-07
Second private release to a small group of testers

- Fixed: autocorrection on non cluster / list markers;
- Added: loader on map changes / loading of info windows;
- Added: ability to load a tab through ajax when needed;
- Added: map manager which handles the loading of the librarys needed;
- Fixed: several problems in IE7;
- Added: geocode latitude / longitude to DMS conversions;
- Changed: rewrote / restructured the demos; 

===================

v0.1 - 2010-01-08
First private release to a small group of testers 

=== TODO ===
- Creating wrapper around polygon marking of for example zipcodes / areas /citys etc...
- Create documentation videos;
- Create api documentation;
- Test cross browser / platform;

=== KNOWN ISSUES ===
- Clustering on distance / address not working perfect yet (needs fixing in some overlapping areas);
- Calculating of the Center Geocode in bounds not working properly when crossing Australia to America;

=== OPEN REQUESTS ===
By Greg Garland:
- Add support for custom layers like wikipedia etc;
- Adding of custom buttons with drop down features (like the wikipedia etc..);
- Add the ability to get the closest x of markers around a selected marker;

=== TODO ===
- Fix address based clustering clustering on a adress type below if one on te top is not available;
  
=== Building ===
This module is built using the Require.js built system, more info:
https://github.com/jrburke/r.js/

1. Install node from https://nodejs.org/en/
2. In the command line go to the tools directory

== Building the module ==
1. Run the command r.js -o svzsolutions.build.js from the tools directory
2. Copy the all.js file from the build directory;

More info:
http://requirejs.org/docs/optimization.html
https://github.com/requirejs/example-multipage

=== Building documentation ===
The javascript documentation is built using the JSDoc system, more info:
http://jsdoc.sourceforge.net/

2. In the command line go to the extracted js_doc_toolkit directory where jsrun.jar is in.
3. Type in the following to built (changing the paths ofcourse)

java -jar jsrun.jar app/run.js -c=../opensource/svzgooglemaps/inc/svzsolutions/svzsolutions.doc.js








 