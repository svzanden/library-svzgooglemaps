/**
 * Copyright (c) 2013-2017, SVZ Solutions All Rights Reserved.
 * Available via BSD license, see license file included for details.
 *
 * @title:                SVZ Solutions Maps
 * @authors:           Stefan van Zanden <info@svzsolutions.nl>
 * @company:          SVZ Solutions
 * @contributers:
 * @version:          0.7.3
 * @versionDate:      2017-05-10
 * @date:             2017-05-10
 */
//Load common code that includes config, then load the app logic for this page.
//require(['./all'], function (common) {
    require(['svzsolutions/maps/MapManager']);
//});