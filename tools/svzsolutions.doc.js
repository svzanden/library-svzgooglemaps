/**
 * Copyright (c) 2017, SVZ Solutions All Rights Reserved.
 * Available via BSD license, see license file included for details.
 * 
 * @title:				SVZ Solutions Documentation configuration file				
 * @description:  Configuration file, used for building the javascript documentation of the svzsolutions package
 * @authors:   		Stefan van Zanden <info@svzsolutions.nl>
 * @company:  		SVZ Solutions
 * @contributers:	
 * @version:  		0.7.2
 * @versionDate:	2017-05-10
 * @date:     		2017-05-10
 */
{
	// source files to use
	_: [
	    '../opensource/svzgooglemaps/inc/svzsolutions/generic/Geocode.js',
	    '../opensource/svzgooglemaps/inc/svzsolutions/generic/Loader.js',
	    '../opensource/svzgooglemaps/inc/svzsolutions/generic/Marker.js',
	    '../opensource/svzgooglemaps/inc/svzsolutions/generic/MarkerCluster.js',
	    '../opensource/svzgooglemaps/inc/svzsolutions/generic/MarkerList.js',
	    '../opensource/svzgooglemaps/inc/svzsolutions/generic/MarkerCache.js',
	    '../opensource/svzgooglemaps/inc/svzsolutions/generic/MarkerManager.js',
	    '../opensource/svzgooglemaps/inc/svzsolutions/generic/Math.js',
	    '../opensource/svzgooglemaps/inc/svzsolutions/generic/RequestManager.js',
	    '../opensource/svzgooglemaps/inc/svzsolutions/maps/MapManager.js',
	    '../opensource/svzgooglemaps/inc/svzsolutions/maps/googlemaps/Address.js',
	    '../opensource/svzgooglemaps/inc/svzsolutions/maps/googlemaps/CustomOverlay.js',
	    '../opensource/svzgooglemaps/inc/svzsolutions/maps/googlemaps/InfoWindow.js',
	    '../opensource/svzgooglemaps/inc/svzsolutions/maps/googlemaps/Map.js',
	    ],
	
	// document all functions, even uncommented ones
	a: false,
	
	// Excluding those marked @private / underscored
	p: false, // Set to false in public documentation, no point in using internal methods by the user
	
	// some extra variables I want to include
	D: { generatedBy: "Stefan van Zanden", copyright: "2017"},
	
	// use this directory as the output directory
	d: "docs/api/js",
	
	// Don't need to do recursive we will just supply the files that are needed manually (in this files top section _ var)
	r: false,
	
	n: true,
	
	// Use UTF-8 encoding
	e: 'UTF-8',
	
	// use this template
	t: "templates/jsdoc",
}