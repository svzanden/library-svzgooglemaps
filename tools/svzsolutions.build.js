/**
 * Main build file for the svzmaps project 
 */
{
	appDir: '../inc',
	baseUrl: 'libraries',
    paths: {
        svzsolutions: '../svzsolutions'
    },
    dir: '../build',
    keepBuildDir: false,
    optimize: "uglify2",
    preserveLicenseComments: false,
    modules: [
        // Setup the all layer
        {
        	name: '../all'
        }
    ]
}