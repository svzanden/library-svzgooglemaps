<?php

  /**
   * Copyright (c) 2010, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Moderate demo 7
   * @description:  This demo shows the use of streetview on a map.
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.6.2
   * @versionDate:  2010-11-07
   * @date:         2010-11-07
   */

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/maps/Map.php');

  // Create a new instance of Google Maps version 3
  $map                          = SVZ_Solutions_Maps_Map::getInstance(SVZ_Solutions_Maps_Map::MAP_TYPE_GOOGLE_MAPS, '3');
  $map->setWidth(500);
  $map->setHeight(400);

  // Sets the id of the container (HTMLDomElement) the map must be put on.
  $map->setContainerId('map');

  // Sets the default map type to satellite
  $map->setMapType('roadmap');

  // Enable the street view control
  $map->setControlStreetView(true);

  // Sets the zoom level to start with to 8.
  $map->setZoomLevel(8);

  // Sets the geocode the map should start at centered.
  //$map->setCenterGeocode(new SVZ_Solutions_Generic_Geocode(51.5479672, 5.648499));
  $map->setCenterGeocode(new SVZ_Solutions_Generic_Geocode(51.697528, 5.335596));

?>
<!DOCTYPE HTML>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>SVZ Solutions - Maps - Moderat demo 7</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
  <link rel="stylesheet" type="text/css" href="../../../inc/css/svzmaps.css" media="all" />
  <link rel="stylesheet" type="text/css" href="../../inc/css/svzmaps-your.css" media="all" />

  <!--[if lte IE 9]>
  <link rel="stylesheet" type="text/css" href="../../inc/css/svzmaps-your-ie.css" media="all" />
  <![endif]-->

  <!-- BEGIN: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->
  <link rel="stylesheet" type="text/css" href="../../inc/css/demo.css" media="all" />
  <!-- END: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->

  <?php

    $requires = array('yourlib/maps/googlemaps/ExtensionModerateDemo7');

    $initOnLoad = "var mapManager  = new svzsolutions.maps.MapManager();

      // The SVZ_Solutions_Maps_Google_Maps_Map php class will generate a config object depending on your settings for you,
      // this generated object can be encoded into a JSON string and can be put encoded into the svzsolutions.maps.MapManager object.
      var map         = mapManager.initByConfig('" . json_encode($map->getConfig()) . "');

      // Initialize your extensions
      var extension   = new yourlib.maps.googlemaps.ExtensionModerateDemo7(map);

      // Startup all the maps (call after subscribing within your extensions)
      mapManager.startup();";

  ?>
</head>
<body>

  <div class="demo-information">
    <h1>Moderate demo 7: Usage of the street view control</h1>
    <p><a href="../../index.html">&#171; back to demo overview</a></p>
    <p>This demo shows the use of the street view control.<br />
    More info on these controls can be found <a href="#">here *TODO*</a></p>
  </div>

	<div style="float: right; width: 600px;">
		<div id="panorama" style="width: 500px;height: 400px;margin: 10px;"></div>
		<div id="panoramaSettings" style="width: 200px;height: 300px;margin: 10px;">

		</div>
	</div>

  <div id="<?php echo $map->getContainerId(); ?>" class="map-holder" style="width: <?php echo $map->getWidth(); ?>px; height: <?php echo $map->getHeight(); ?>px"></div>

<?php require_once('../../footer.phtml'); ?>