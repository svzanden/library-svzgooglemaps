<?php

  /**
   * Copyright (c) 2009, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Basic demo 2
   * @description:  This demo data file retrieves a set of data marker positions from the
   *                testdata array and imports them into the markermanager.
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.2
   * @versionDate:  2010-02-06
   * @date:         2010-02-06
   */

  // Including of some sample data
  require_once('../../testdata/data.php');

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/generic/MarkerManager.php');

  $mapClusterMode   = SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_NONE;

  $markerManager    = new SVZ_Solutions_Generic_Marker_Manager();
  $markerManager->setClusterMode($mapClusterMode);
  $markerManager->import($data);

  // Generate JSON output
  $output           = new StdClass();
  $output->markers  = $markerManager->toArray();

  echo json_encode($output);

?>