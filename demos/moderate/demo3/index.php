<?php

  /**
   * Copyright (c) 2009, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Moderate demo 3
   * @description:  This demo shows how to retrieve the geocodes of a marker and the ability to drag it around.
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.2
   * @versionDate:  2010-02-06
   * @date:         2010-02-06
   */

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/maps/Map.php');

  $map                          = SVZ_Solutions_Maps_Map::getInstance(SVZ_Solutions_Maps_Map::MAP_TYPE_GOOGLE_MAPS, '3');
  $map->setWidth(900);
  $map->setHeight(600);

  // Sets the id of the container (HTMLDomElement) the map must be put on.
  $map->setContainerId('map');

  // Sets the default map type to roadmap.
  $map->setMapType('roadmap');

  // Sets the zoom level to start with to 8.
  $map->setZoomLevel(8);

  // Sets the geocode the map should start at centered.
  $map->setCenterGeocode(new SVZ_Solutions_Generic_Geocode(51.5479672, 5.648499));

  // Add a admin marker type
  $markerType = new SVZ_Solutions_Generic_Marker_Type('admin');
  $markerType->enableIcon();
  $markerType->getLayer()->setName('admin'); // Define a layer where the provided marker types should live in
  $markerType->getLayer()->setTypeFixed();
  $map->addMarkerType($markerType);

  // Add a single admin marker
  $marker     = new SVZ_Solutions_Generic_Marker('admin', 51.5479672, 5.648499);
  $marker->setDraggable(true);
  $marker->setContent('Drag me!');
  $map->addMarker($marker);

?>
<!DOCTYPE HTML>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>SVZ Solutions - Maps - Moderate demo 3</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
  <link rel="stylesheet" type="text/css" href="../../../inc/css/svzmaps.css" media="all" />
  <link rel="stylesheet" type="text/css" href="../../inc/css/svzmaps-your.css" media="all" />

  <!--[if lte IE 9]>
  <link rel="stylesheet" type="text/css" href="../../inc/css/svzmaps-your-ie.css" media="all" />
  <![endif]-->

  <!-- BEGIN: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->
  <link rel="stylesheet" type="text/css" href="../../inc/css/demo.css" media="all" />
  <!-- END: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->

  <?php

    $requires   = array( 'yourlib/maps/googlemaps/ExtensionModerateDemo3' );

    $initOnLoad = "var mapManager  = new svzsolutions.maps.MapManager();

      // The SVZ_Solutions_Maps_Google_Maps_Map php class will generate a config object depending on your settings for you,
      // this generated object can be encoded into a JSON string and can be put encoded into the svzsolutions.maps.MapManager object.
      var map         = mapManager.initByConfig('" . json_encode($map->getConfig()) . "');

      // Initialize your extensions
      var extension   = new yourlib.maps.googlemaps.ExtensionModerateDemo3(map);

      // Startup all the maps (call after subscribing within your extensions)
      mapManager.startup();";

  ?>
</head>
<body>

  <div class="demo-information">
    <h1>Moderate demo 3: Administrating marker positions.</h1>
    <p><a href="../../index.html">&#171; back to demo overview</a></p>
    <p>This demo shows how to drag a marker around or typing in some geocoordinates to update a marker position.<br />
    More info about this can be found <a href="#">here *TODO*</a>.</p>
  </div>

  <div class="demo-settings">
    <form method="post">
      <div class="columns-holder">

        <div class="column">

          <div class="row">
            <div class="label">
              <label for="marker-geocode-latitude">Latitude:</label>
            </div>
            <div class="value">
              <input type="text" id="marker-geocode-latitude" name="marker-geocode-latitude" value="<?php echo $marker->getGeocode()->getLatitude(); ?>" />
            </div>
          </div>

          <div class="row">
            <div class="label">
              <label for="marker-geocode-longitude">Longitude:</label>
            </div>
            <div class="value">
              <input type="text" id="marker-geocode-longitude" name="marker-geocode-longitude" value="<?php echo $marker->getGeocode()->getLongitude(); ?>" />
            </div>
          </div>

        </div>

        <div class="column">

          <div class="row">
            <div class="label">
              Latitude DMS:
            </div>
            <div class="value">
              <span id="marker-geocode-latitude-dms"><?php echo $marker->getGeocode()->getLatitudeInDMS(); ?></span>
            </div>
          </div>

          <div class="row">
            <div class="label">
              Longitude DMS:
            </div>
            <div class="value">
              <span id="marker-geocode-longitude-dms"><?php echo $marker->getGeocode()->getLongitudeInDMS(); ?></span>
            </div>
          </div>

         </div>

       </div>

    </form>
  </div>

  <div id="<?php echo $map->getContainerId(); ?>" class="map-holder" style="width: <?php echo $map->getWidth(); ?>px; height: <?php echo $map->getHeight(); ?>px"></div>

<?php require_once('../../footer.phtml'); ?>