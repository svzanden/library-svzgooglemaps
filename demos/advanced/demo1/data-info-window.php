<?php

  /**
   * Copyright (c) 2009, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Advanced demo 1
   * @description:  This demo data file retrieves the content for a info window.
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.2
   * @versionDate:  2010-02-06
   * @date:         2010-02-06
   */

  // Including of some sample data
  require_once('../../testdata/data.php');


  // Get parameters from URL
  $entityIds    = isset($_GET['entityIds']) ? explode(',', $_GET['entityIds']) : array();
  $type         = isset($_GET['type']) ? $_GET['type'] : '';

  $mode         = 'normal';

  if (count($entityIds) > 0)
    $mode = 'list';


  if ($mode == 'list')
  {
    // Including of the SVZ Solutions library
    require_once('../../../includes/svzsolutions/generic/InfoWindowContentList.php');

    $projectId    = $entityIds[0];

    $infoWindow = new SVZ_Solutions_Generic_Info_Window_Content_List();

    $infoWindow->addClassName('list');

    // Find all the entity Ids and generate a list marker for it.
    foreach ($data as $key => $value)
    {
      if (in_array($value['entityId'], $entityIds))
      {
        $listItem = '<a href="' . $value['dataLoadUrl'] . '">';

        $listItem .= '<img alt="" src="../../inc/img/woning.jpg" width="90" /><br />';

        $listItem .= $value['title'];

        $listItem .= '</a><br /><br />';

        $infoWindow->addListItemHtml($listItem);
      }

    }
  }
  else
  {
    // Including of the SVZ Solutions library
    require_once('../../../includes/svzsolutions/generic/InfoWindowContent.php');

    // Get parameters from URL
    $projectId    = isset($_GET['id']) ? $_GET['id'] : 0;
    $type         = isset($_GET['type']) ? $_GET['type'] : '';

    $infoWindow = new SVZ_Solutions_Generic_Info_Window_Content();

    $infoWindow->addClassName('type-' . strtolower($type));
  }


  // Find the marker in the sample data array by its id
  foreach ($data as $key => $value)
  {
    if ($value['entityId'] == $projectId)
      $result[] = $value;

  }

  $output = array();

  if ($result)
  {
    foreach ($result as $project)
    {
      $projectAddress                 = $project['address'];

      $price                          = '';

      // Defining the way the header looks like
      $headerHtml = '<img class="main-image" alt="" src="../../inc/img/woning.jpg" />
               <h2>' . $project['title'] . '</h2>
               <h3>' . $price . '</h3>';

      $infoWindow->setHeaderHtml($headerHtml);

      $propertiesTab = '<p>Properties</p>';

      $textTab = '';


      $mediaTab = '';
      $mediaTab .= '<a href="#" class="dialog">&raquo; Toon meer afbeeldingen</a><br />';
      $mediaTab .= '<a href="#" class="dialog">&raquo; Toon videos</a><br />';
      $mediaTab .= '<a href="#" class="dialog">&raquo; Toon plattegronden</a><br />';
      $mediaTab .= '<a href="#" class="dialog">&raquo; Toon 360 graden presentatie</a><br />';

      $actionsTab = '<ul>' .
                      '<li><a href="#">&raquo; Voeg toe aan vergelijking</a></li>' .
                      '<li><a href="#">&raquo; Reageer op dit object</a></li>' .
                      '<li><a href="#">&raquo; Informeer een bekende</a></li>' .
                      '<li><a href="#">&raquo; Print object</a></li>' .
                      '<li><a class="external-link" href="#">&raquo; Reserveer woning</a></li>' .
                      '<li><a class="external-link" href="#">&raquo; Monitor woning</a></li>' .
                    '</ul>';



      $infoWindow->addTab('Kenmerken', $propertiesTab);
      $infoWindow->addTab('Teksten', $textTab, 'data-info-window-texts.php');
      $infoWindow->addTab('Media', $mediaTab);
      $infoWindow->addTab('Acties op deze woning', $actionsTab);

      $output['content'] = $infoWindow->getHTML();
    }

  }
  else
  {
    $output['content'] = 'Could not find the project data.';
  }

  echo json_encode($output);

?>