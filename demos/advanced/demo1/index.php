<?php

  /**
   * Copyright (c) 2009, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Advanced demo 1
   * @description:  This demo shows distance based clustering and the use of list info windows.
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.2
   * @versionDate:  2010-02-06
   * @date:         2010-02-06
   */

  // Including of some sample data
  require_once('../../testdata/data.php');

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/maps/Map.php');

  $map                          = SVZ_Solutions_Maps_Map::getInstance(SVZ_Solutions_Maps_Map::MAP_TYPE_GOOGLE_MAPS, '3');
  $map->setWidth(900);
  $map->setHeight(600);

  // Sets the id of the container (HTMLDomElement) the map must be put on.
  $map->setContainerId('map');

  // Set the absolute or relative location of the file which will return your data like markers.
  $map->setDataLoadUrl('data.php');

  // Sets the default map type to roadmap.
  $map->setMapType('roadmap');

  // Sets the zoom level to start with to 8.
  $map->setZoomLevel(8);

  // Sets the geocode the map should start at centered.
  $map->setCenterGeocode(new SVZ_Solutions_Generic_Geocode(51.5479672, 5.648499));

  // Create for each type a marker type
  foreach ($types as $type)
  {
    // Add a type marker type
    $markerType = new SVZ_Solutions_Generic_Marker_Type($type);
    $map->addMarkerType($markerType);
  }

  // Add a cluster marker type
  $markerType = new SVZ_Solutions_Generic_Marker_Type(SVZ_Solutions_Generic_Marker_Type::MARKER_TYPE_CLUSTER);
  $markerType->setClickAction('zoom');

  $map->addMarkerType($markerType);

  // Add a list marker type
  $markerType = new SVZ_Solutions_Generic_Marker_Type(SVZ_Solutions_Generic_Marker_Type::MARKER_TYPE_LIST);
  $map->addMarkerType($markerType);

?>
<!DOCTYPE HTML>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>SVZ Solutions - Maps - Advanced demo 1</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
  <link rel="stylesheet" type="text/css" href="../../../inc/css/svzmaps.css" media="all" />
  <link rel="stylesheet" type="text/css" href="../../inc/css/svzmaps-your.css" media="all" />

  <!--[if lte IE 9]>
  <link rel="stylesheet" type="text/css" href="../../inc/css/svzmaps-your-ie.css" media="all" />
  <![endif]-->

  <!-- BEGIN: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->
  <link rel="stylesheet" type="text/css" href="../../inc/css/demo.css" media="all" />
  <!-- END: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->

  <?php

    $initOnLoad = "var mapManager  = new svzsolutions.maps.MapManager();

      // The SVZ_Solutions_Maps_Google_Maps_Map php class will generate a config object depending on your settings for you,
      // this generated object can be encoded into a JSON string and can be put encoded into the svzsolutions.maps.MapManager object.
      var map         = mapManager.initByConfig('" . json_encode($map->getConfig()) . "');

      // Startup all the maps (call after subscribing within your extensions)
      mapManager.startup();";

  ?>
</head>
<body>

  <div class="demo-information">
    <h1>Advanced demo 1: Clustering markers distance based and info window list</h1>
    <p><a href="../../index.html">&#171; back to demo overview</a></p>
    <p>This demo shows distance based clustering and the use of list info windows.<br />
    More info about this can be found <a href="#">here *TODO*</a>.</p>
  </div>

  <div id="<?php echo $map->getContainerId(); ?>" class="map-holder" style="width: <?php echo $map->getWidth(); ?>px; height: <?php echo $map->getHeight(); ?>px"></div>

<?php require_once('../../footer.phtml'); ?>