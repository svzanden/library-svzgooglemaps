<?php

  /**
   * Copyright (c) 2010, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Advanced demo 1
   * @description:  This demo data file retrieves the content for a info window.
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.5
   * @versionDate:  2010-05-30
   * @date:         2010-05-30
   */

  $response = new StdClass();
  $response->content = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla cursus, lacus id rhoncus congue, nunc odio lobortis purus, sed interdum neque sem quis risus. Vivamus neque diam, commodo vitae imperdiet at, sodales tempus leo. Morbi sollicitudin rutrum aliquam. Fusce lacinia mi non erat sagittis a pellentesque augue scelerisque. Duis vitae dui turpis, non euismod urna. Ut lacinia eleifend justo, sed posuere quam ornare pretium. Donec eu fringilla dolor. Pellentesque vestibulum malesuada faucibus. Quisque scelerisque, arcu semper aliquam pharetra, dolor ligula feugiat ante, vel blandit tellus arcu quis erat. Aliquam egestas tempor turpis ac suscipit. Maecenas imperdiet tristique ornare. Donec accumsan ultricies ante, eu molestie lorem fermentum sit amet. Sed vel justo lacus, eu sagittis nulla. Nullam vel libero in arcu consequat malesuada ac nec risus.</p>';
  $response->content .= '<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus eu est eros, at porttitor sem. Curabitur sit amet sapien magna. Nam sit amet felis sed lectus blandit rutrum. Sed leo urna, mattis eu ornare sed, condimentum eget eros. Donec lobortis sollicitudin mi, porttitor fringilla massa tincidunt sit amet. Pellentesque tincidunt risus vitae velit tempor aliquam. Aenean eu sodales lacus. Aliquam condimentum ligula vel augue iaculis at tristique dui porta. Fusce pellentesque, ipsum vel consectetur sollicitudin, purus mauris scelerisque sem, et placerat eros neque ut justo. Pellentesque vel dui sapien, ut fermentum nisl. Duis in orci diam. In dolor lectus, eleifend eget luctus nec, adipiscing ut ante. Mauris suscipit lacus ac nibh auctor at pretium sapien venenatis. </p>';

  echo json_encode($response);

?>