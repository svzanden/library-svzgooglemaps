<?php

  session_start();

  $mapClusterMode             = isset($_GET['map_cluster_mode']) ? $_GET['map_cluster_mode'] : false;
  $mapWidth                   = isset($_GET['map_width']) ? $_GET['map_width'] : false;
  $mapHeight                  = isset($_GET['map_height']) ? $_GET['map_height'] : false;
  $mapZoomLevel               = isset($_GET['map_zoom_level']) ? $_GET['map_zoom_level'] : false;
  $mapCenterGeocodeLatitude   = isset($_GET['map_center_geocode_latitude']) ? $_GET['map_center_geocode_latitude'] : false;
  $mapCenterGeocodeLongitude  = isset($_GET['map_center_geocode_longitude']) ? $_GET['map_center_geocode_longitude'] : false;
  $mapTypeControlStyle        = isset($_GET['map_type_control_style']) ? $_GET['map_type_control_style'] : false;
  $mapNavigationControlStyle  = isset($_GET['map_navigation_control_style']) ? $_GET['map_navigation_control_style'] : false;

  // Temporary storage in SESSION
  if ($mapClusterMode)
    $_SESSION['map_cluster_mode'] = $mapClusterMode;

  if ($mapWidth)
    $_SESSION['map_width'] = $mapWidth;

  if ($mapHeight)
    $_SESSION['map_height'] = $mapHeight;

  if ($mapZoomLevel)
    $_SESSION['map_zoom_level'] = $mapZoomLevel;

  if ($mapCenterGeocodeLatitude)
    $_SESSION['map_center_geocode_latitude'] = $mapCenterGeocodeLatitude;

  if ($mapCenterGeocodeLongitude)
    $_SESSION['map_center_geocode_longitude'] = $mapCenterGeocodeLongitude;

  if ($mapTypeControlStyle)
    $_SESSION['map_type_control_style'] = $mapTypeControlStyle;

  if ($mapNavigationControlStyle)
    $_SESSION['map_navigation_control_style'] = $mapNavigationControlStyle;

?>