<?php

  /**
   * Copyright (c) 2009, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Advanced demo 3
   * @description:  This demo shows administrating of a map, changing it's settings on the fly.
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.2
   * @versionDate:  2010-02-07
   * @date:         2010-02-07
   */

  session_start();

  // Including of some sample data
  require_once('../../testdata/data.php');

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/maps/Map.php');

  $mapClusterMode               = !empty($_SESSION['map_cluster_mode']) ? $_SESSION['map_cluster_mode'] : SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_DISTANCE;
  $mapWidth                     = !empty($_SESSION['map_width']) ? $_SESSION['map_width'] : 800;
  $mapHeight                    = !empty($_SESSION['map_height']) ? $_SESSION['map_height'] : 600;
  $mapCenterGeocodeLatitude     = !empty($_SESSION['map_center_geocode_latitude']) ? (float)$_SESSION['map_center_geocode_latitude'] : 51.5479672;
  $mapCenterGeocodeLongitude    = !empty($_SESSION['map_center_geocode_longitude']) ? (float)$_SESSION['map_center_geocode_longitude'] : 5.648499;
  $mapZoomLevel                 = !empty($_SESSION['map_zoom_level']) ? (int)$_SESSION['map_zoom_level'] : 8;
  $mapTypeControlStyle          = !empty($_SESSION['map_type_control_style']) ? $_SESSION['map_type_control_style'] : 'default';
  $mapNavigationControlStyle    = !empty($_SESSION['map_navigation_control_style']) ? $_SESSION['map_navigation_control_style'] : 'default';
  $debugMode                    = isset($_GET['debugMode']) ? true : false;

  $map                          = SVZ_Solutions_Maps_Map::getInstance(SVZ_Solutions_Maps_Map::MAP_TYPE_GOOGLE_MAPS, '3');
  $map->setWidth($mapWidth);
  $map->setHeight($mapHeight);

  // Sets the id of the container (HTMLDomElement) the map must be put on.
  $map->setContainerId('map');

  // Set the absolute or relative location of the file which will return your data like markers.
  $map->setDataLoadUrl('data.php');

  // Sets the default map type to roadmap.
  $map->setMapType('roadmap');

  // Sets the zoom level to start with to 8.
  $map->setZoomLevel($mapZoomLevel);

  // Sets the geocode the map should start at centered.
  $map->setCenterGeocode(new SVZ_Solutions_Generic_Geocode($mapCenterGeocodeLatitude, $mapCenterGeocodeLongitude));

  // Sets the type control.
  $map->setControlMapType(true, SVZ_Solutions_Maps_Google_Maps_Map::CONTROL_DEFAULT_MAP_TYPE_POSITION, $mapTypeControlStyle);

  // Sets the navigations control style.
  //$map->setControlMapNavigation(true, SVZ_Solutions_Maps_Google_Maps_Map::CONTROL_DEFAULT_MAP_NAVIGATION_POSITION, $mapNavigationControlStyle);

  // Create for each type a marker type
  foreach ($types as $type)
  {
    // Add a type marker type
    $markerType = new SVZ_Solutions_Generic_Marker_Type($type);

    /* Old icon way, should not be used in case of alot of the same marker types
    $markerType->setIconUrl('http://labs.google.com/ridefinder/images/mm_20_blue.png');
    $markerType->setIconShadowUrl('http://labs.google.com/ridefinder/images/mm_20_shadow.png');
    */

    $map->addMarkerType($markerType);
  }

  // Add a cluster marker type
  $markerType = new SVZ_Solutions_Generic_Marker_Type(SVZ_Solutions_Generic_Marker_Type::MARKER_TYPE_CLUSTER);
  $markerType->setClickAction('zoom');

  $map->addMarkerType($markerType);

  // Add a list marker type
  $markerType = new SVZ_Solutions_Generic_Marker_Type(SVZ_Solutions_Generic_Marker_Type::MARKER_TYPE_LIST);
  $map->addMarkerType($markerType);

?>
<!DOCTYPE HTML>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>SVZ Solutions - Maps - Advanced demo 3</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
  <link rel="stylesheet" type="text/css" href="../../../inc/css/svzmaps.css" media="all" />
  <link rel="stylesheet" type="text/css" href="../../inc/css/svzmaps-your.css" media="all" />

  <!--[if lte IE 9]>
  <link rel="stylesheet" type="text/css" href="../../inc/css/svzmaps-your-ie.css" media="all" />
  <![endif]-->

  <!-- BEGIN: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->
  <link rel="stylesheet" type="text/css" href="../../inc/css/demo.css" media="all" />
  <!-- END: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->

  <?php

    $requires = array('yourlib/maps/googlemaps/ExtensionAdvancedDemo3');

    $initOnLoad = "var mapManager  = new svzsolutions.maps.MapManager();

      // The SVZ_Solutions_Maps_Google_Maps_Map php class will generate a config object depending on your settings for you,
      // this generated object can be encoded into a JSON string and can be put encoded into the svzsolutions.maps.MapManager object.
      var map         = mapManager.initByConfig('" . json_encode($map->getConfig()) . "');

      // Initialize your extensions
      var extension   = new yourlib.maps.googlemaps.ExtensionAdvancedDemo3(map);

      // Startup all the maps (call after subscribing within your extensions)
      mapManager.startup();";

  ?>
</head>
<body>

  <div class="demo-information">
    <h1>Advanced demo 3: Administrating a maps functionality.</h1>
    <p><a href="../../index.html">&#171; back to demo overview</a></p>
    <p>This demo shows administrating of a map, changing it's settings on the fly.<br />
    More info about this can be found <a href="#">here *TODO*</a>.</p>
  </div>

  <div class="wrapper">
    <div class="sg-maps-control-panel">
      <h2>Admin panel</h2><br /><br />
      <form method="get" action="#">

        <div class="row">
          <div class="column-left">
              Cluster mode:
          </div>
          <div class="column-right">
            <select name="map_cluster_mode">
              <option value="<?php echo SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_NONE . ($mapClusterMode == SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_NONE ? '" selected="selected' : ''); ?>">None</option>
              <option value="<?php echo SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_DISTANCE . ($mapClusterMode == SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_DISTANCE ? '" selected="selected' : ''); ?>">Distance</option>
              <option value="<?php echo SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_ADDRESS . ($mapClusterMode == SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_ADDRESS ? '" selected="selected' : ''); ?>">Address</option>
            </select>
          </div>
        </div>
        <br />

        <div class="row">
          <div class="column-left">
              Map width:
          </div>
          <div class="column-right">
            <input name="map_width" type="text" value="<?php echo $mapWidth; ?>" />
          </div>
        </div>

        <div class="row">
          <div class="column-left">
              Map height:
          </div>
          <div class="column-right">
            <input name="map_height" type="text" value="<?php echo $mapHeight; ?>" />
          </div>
        </div>

        <div class="row">
          <div class="column-left">
              Zoom level:
          </div>
          <div class="column-right">
            <select name="map_zoom_level">
              <?php

                $maxZoomLevel = 19;

                for ($i = 1; $i < $maxZoomLevel; $i++)
                {
                  $selected = '';

                  if ($i == $mapZoomLevel)
                    $selected = ' selected="selected"';

                  echo '<option value="' . $i . '"' . $selected . '>' . $i . '</option>';
                }

              ?>
            </select>
          </div>
        </div>

        <div class="row">
          <div class="column-left">
              Center latitude:
          </div>
          <div class="column-right">
            <input name="map_center_geocode_latitude" type="text" value="<?php echo $mapCenterGeocodeLatitude; ?>" />
          </div>
        </div>

        <div class="row">
          <div class="column-left">
              Center longitude:
          </div>
          <div class="column-right">
            <input name="map_center_geocode_longitude" type="text" value="<?php echo $mapCenterGeocodeLongitude; ?>" />
          </div>
        </div>

        <br />
        <strong>Store the following options and reload the window before changes will affect:</strong>

        <div class="row">
          <div class="column-left">
              Type Control style:
          </div>
          <div class="column-right">
            <?php

              $mapTypeControlStyles = $map->getControlTypeStyles();

              for ($i = 0; $i < count($mapTypeControlStyles); $i++)
              {
                $checked = '';

                if ($mapTypeControlStyles[$i] == $mapTypeControlStyle)
                  $checked = ' checked="checked"';

                echo '<input type="radio" id="map_type_control_style_' . $mapTypeControlStyles[$i] . '" name="map_type_control_style" value="' . $mapTypeControlStyles[$i] . '"' . $checked . ' /><label for="map_type_control_style_' . $mapTypeControlStyles[$i] . '">' . $mapTypeControlStyles[$i] . '</label><br />';
              }

            ?>
          </div>
        </div>

        <div class="row">
          <div class="column-left">
              Navigation Control style:
          </div>
          <div class="column-right">
            <?php

              $mapNavigationControlStyles = $map->getControlZoomStyles();

              for ($i = 0; $i < count($mapNavigationControlStyles); $i++)
              {
                $checked = '';

                if ($mapNavigationControlStyles[$i] == $mapNavigationControlStyle)
                  $checked = ' checked="checked"';

                echo '<input type="radio" id="map_navigation_control_style_' . $mapNavigationControlStyles[$i] . '" name="map_navigation_control_style" value="' . $mapNavigationControlStyles[$i] . '"' . $checked . ' /><label for="map_navigation_control_style_' . $mapNavigationControlStyles[$i] . '">' . $mapNavigationControlStyles[$i] . '</label><br />';
              }

            ?>
          </div>
        </div>

        <br />

        <br />

        <div class="row">
          <div class="column-left">
            <input type="submit" name="store_settings" value="Keep settings stored" />
          </div>
          <div class="column-right"></div>
        </div>

      </form>

    </div>

    <div id="<?php echo $map->getContainerId(); ?>" style="width: <?php echo $map->getWidth(); ?>px; height: <?php echo $map->getWidth(); ?>px"></div>

  </div>

<?php require_once('../../footer.phtml'); ?>