<?php

  /**
   * Copyright (c) 2009, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Advanced demo 4
   * @description:  This demo data file retrieves a set of data marker positions from the
   *                testdata array and imports them into the markermanager and then returns
   *                them based on the zoom level / distance in a clustered fasion.
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.4
   * @versionDate:  2010-02-28
   * @date:         2010-02-28
   */

  // Including of some sample data
  require_once('../../testdata/data.php');

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/generic/MarkerManager.php');

  // Reading out data applied in the requests
  $mapClusterMode   = SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_DISTANCE;

  $markerManager    = new SVZ_Solutions_Generic_Marker_Manager();
  $markerManager->setListDataLoadUrl('data-info-window-list.php');
  $markerManager->setClusterMode($mapClusterMode);
  $markerManager->enableReturnCountPerMarkerType();
  $markerManager->import($data);

  // Generate JSON output
  $output           = new StdClass();
  $output->markers  = $markerManager->toArray();

  echo json_encode($output);

?>