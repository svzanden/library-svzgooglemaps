<?php

  /**
   * Copyright (c) 2016, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Advanced demo 4
   * @description:  This demo shows distance based clustering and the use of list info windows and
   *                shows the bounds of which the cluster marker is calculated using polylines.
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.7.0
   * @versionDate:  2016-02-21
   * @date:         2016-02-21
   */

  // Including of some sample data
  //require_once('../../testdata/data.php');

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/maps/Map.php');

  $map                          = SVZ_Solutions_Maps_Map::getInstance(SVZ_Solutions_Maps_Map::MAP_TYPE_GOOGLE_MAPS, '3');
  $map->setWidth(90);
  $map->setWidthUnit('vw');
  $map->setHeight(90);
  $map->setHeightUnit('vh');

  // Sets the id of the container (HTMLDomElement) the map must be put on.
  $map->setContainerId('map');

  // Set the absolute or relative location of the file which will return your data like markers.
  //$map->setDataLoadUrl('data.php');

  // Sets the default map type to roadmap.
  $map->setMapType('roadmap');

  // Sets the zoom level to start with to 8.
  $map->setZoomLevel(14);

  // Sets the geocode the map should start at centered.
  $map->setCenterGeocode(new SVZ_Solutions_Generic_Geocode(51.5479672, 5.648499));

  // Add a admin marker type
  $markerType = new SVZ_Solutions_Generic_Marker_Type('admin');
  $markerType->enableIcon();
  $markerType->getLayer()->setName('admin'); // Define a layer where the provided marker types should live in
  $markerType->getLayer()->setTypeStatic();
  $map->addMarkerType($markerType);

  // Add a single admin marker
  $marker     = new SVZ_Solutions_Generic_Marker('admin', 51.5479672, 5.648499);
  $map->addMarker($marker);

?>
<!DOCTYPE HTML>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>SVZ Solutions - Maps - Advanced demo 9</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
  <link rel="stylesheet" type="text/css" href="../../../inc/css/svzmaps.css" media="all" />
  <link rel="stylesheet" type="text/css" href="../../inc/css/svzmaps-your.css" media="all" />

  <!--[if lte IE 9]>
  <link rel="stylesheet" type="text/css" href="../../inc/css/svzmaps-your-ie.css" media="all" />
  <![endif]-->

  <!-- BEGIN: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->
  <link rel="stylesheet" type="text/css" href="../../inc/css/demo.css" media="all" />
  <!-- END: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->

  <?php

    $requires = array('yourlib/maps/googlemaps/ExtensionAdvancedDemo9');

    $initOnLoad = "var mapManager  = new svzsolutions.maps.MapManager();

      // The SVZ_Solutions_Maps_Google_Maps_Map php class will generate a config object depending on your settings for you,
      // this generated object can be encoded into a JSON string and can be put encoded into the svzsolutions.maps.MapManager object.
      var map         = mapManager.initByConfig('" . json_encode($map->getConfig()) . "');

      // Initialize your extensions
      var extension   = new yourlib.maps.googlemaps.ExtensionAdvancedDemo9(map);

      // Startup all the maps (call after subscribing within your extensions)
      mapManager.startup();";

  ?>
</head>
<body>

  <div class="demo-information">
    <h1>Advanced demo 9: Google Places API information</h1>
    <p><a href="../../index.html">&#171; back to demo overview</a></p>
    <p>Additional types can be found <a href="https://developers.google.com/places/supported_types">here</a></p>
  </div>

  <div id="<?php echo $map->getContainerId(); ?>" class="map-holder" style="width: <?php echo $map->getWidth() . $map->getWidthUnit(); ?>; height: <?php echo $map->getHeight() . $map->getHeightUnit(); ?>"></div>

<?php require_once('../../footer.phtml'); ?>