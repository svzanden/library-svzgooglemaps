<?php

  /**
   * Copyright (c) 2010, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Moderate demo 3
   * @description:  This demo shows how to put multiple maps on 1 page and use multiple extensions asswel as the same ones.
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.6
   * @versionDate:  2010-11-01
   * @date:         2010-11-01
   */

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/maps/Map.php');

  // MAP number 1
  $map1                          = SVZ_Solutions_Maps_Map::getInstance(SVZ_Solutions_Maps_Map::MAP_TYPE_GOOGLE_MAPS, '3');
  $map1->setWidth(400);
  $map1->setHeight(300);

  // Sets the id of the container (HTMLDomElement) the map must be put on.
  $map1->setContainerId('map1');

  // Sets the default map type to roadmap.
  $map1->setMapType('roadmap');

  // Sets the zoom level to start with to 8.
  $map1->setZoomLevel(8);

  // Sets the geocode the map should start at centered.
  $map1->setCenterGeocode(new SVZ_Solutions_Generic_Geocode(51.5479672, 5.648499));

  // Add a admin marker type
  $markerType = new SVZ_Solutions_Generic_Marker_Type('admin');
  $markerType->enableIcon();
  $markerType->getLayer()->setName('admin'); // Define a layer where the provided marker types should live in
  $markerType->getLayer()->setTypeFixed();
  $map1->addMarkerType($markerType);

  // Add a single admin marker
  $marker     = new SVZ_Solutions_Generic_Marker('admin', 51.5479672, 5.648499);
  $marker->setDraggable(true);
  $marker->setContent('Drag me!');
  $map1->addMarker($marker);

  // MAP number 2
  $map2                          = SVZ_Solutions_Maps_Map::getInstance(SVZ_Solutions_Maps_Map::MAP_TYPE_GOOGLE_MAPS, '3');
  $map2->setWidth(400);
  $map2->setHeight(300);

  // Sets the id of the container (HTMLDomElement) the map must be put on.
  $map2->setContainerId('map2');

  // Sets the zoom level to start with to 8.
  $map2->setZoomLevel(8);

  // Sets the geocode the map should start at centered.
  $map2->setCenterGeocode(new SVZ_Solutions_Generic_Geocode(51.5479672, 5.648499));

  // MAP number 3
  $map3                          = SVZ_Solutions_Maps_Map::getInstance(SVZ_Solutions_Maps_Map::MAP_TYPE_GOOGLE_MAPS, '3');
  $map3->setWidth(400);
  $map3->setHeight(300);

  // Sets the id of the container (HTMLDomElement) the map must be put on.
  $map3->setContainerId('map3');

  // Sets the default map type to terrain.
  $map3->setMapType('terrain');

  // Sets the zoom level to start with to 8.
  $map3->setZoomLevel(8);

  // Sets the geocode the map should start at centered.
  $map3->setCenterGeocode(new SVZ_Solutions_Generic_Geocode(51.5479672, 5.648499));

?>
<!DOCTYPE HTML>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>SVZ Solutions - Maps - Moderate demo 3</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
  <link rel="stylesheet" type="text/css" href="../../../inc/css/svzmaps.css" media="all" />
  <link rel="stylesheet" type="text/css" href="../../inc/css/svzmaps-your.css" media="all" />

  <!--[if lte IE 9]>
  <link rel="stylesheet" type="text/css" href="../../inc/css/svzmaps-your-ie.css" media="all" />
  <![endif]-->

  <!-- BEGIN: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->
  <link rel="stylesheet" type="text/css" href="../../inc/css/demo.css" media="all" />
  <!-- END: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->

  <?php

    $requires = array('yourlib/maps/googlemaps/ExtensionAdvancedDemo8m1', 'yourlib/maps/googlemaps/ExtensionAdvancedDemo8m3', 'yourlib/maps/googlemaps/ExtensionAdvancedDemo8Center');

    $initOnLoad = "var mapManager  = new svzsolutions.maps.MapManager();

      // The SVZ_Solutions_Maps_Google_Maps_Map php class will generate a config object depending on your settings for you,
      // this generated object can be encoded into a JSON string and can be put encoded into the svzsolutions.maps.MapManager object.
      var map1         = mapManager.initByConfig('" . json_encode($map1->getConfig()) . "');
      var map2         = mapManager.initByConfig('" . json_encode($map2->getConfig()) . "');
      var map3         = mapManager.initByConfig('" . json_encode($map3->getConfig()) . "');

      // Initialize your extensions

      // Makes sure the 1st map updates the coordinates when the marker is moved
      var extension1   = new yourlib.maps.googlemaps.ExtensionAdvancedDemo8m1(map1, 1);

   		// Makes sure the 1st map updates the coordinates when the center is changed
      var extension2   = new yourlib.maps.googlemaps.ExtensionAdvancedDemo8Center(map1, 1);

      // Makes sure the 2nd map updates the coordinates when the center is changed
      var extension3   = new yourlib.maps.googlemaps.ExtensionAdvancedDemo8Center(map2, 2);

      // Adds ability to show the hidden map number 3
      var extension4   = new yourlib.maps.googlemaps.ExtensionAdvancedDemo8m3(map3);

      // Startup all the maps (call after subscribing within your extensions)
      mapManager.startup();";

  ?>
  <style type="text/css">

  	.hide-map,
  	.hide-map .map-holder {
  	visibility: hidden;
  	height: 1px!important; /* Needs to be atleast 1px because of Invalid argument in IE */
  	overflow: hidden;
  	}

  </style>
</head>
<body>

  <div class="demo-information">
    <h1>Advanced demo 8: Loading multiple maps on a single page.</h1>
    <p><a href="../../index.html">&#171; back to demo overview</a></p>
    <p>This demo shows how to load multiple maps on a single page and how to use multiple extensions asswel as the same ones.<br />
    More info about this can be found <a href="#">here *TODO*</a>.</p>
  </div>

  <div class="demo-settings">

  	<h2>Map 1</h2>

    <div class="row">
      <div class="label">
        <label for="marker-geocode-latitude1">Latitude:</label>
      </div>
      <div class="value">
        <span id="marker-geocode-latitude1"><?php echo $marker->getGeocode()->getLatitude(); ?></span>
      </div>
    </div>

    <div class="row">
      <div class="label">
        <label for="marker-geocode-longitude1">Longitude:</label>
      </div>
      <div class="value">
        <span id="marker-geocode-longitude1"><?php echo $marker->getGeocode()->getLongitude(); ?></span>
      </div>
    </div>

  </div>

  <div id="<?php echo $map1->getContainerId(); ?>" class="map-holder" style="width: <?php echo $map1->getWidth(); ?>px; height: <?php echo $map1->getHeight(); ?>px"></div>

  <div class="demo-settings">

  	<h2>Map 2</h2>

    <div class="row">
      <div class="label">
        <label for="marker-geocode-latitude2">Latitude:</label>
      </div>
      <div class="value">
        <span id="marker-geocode-latitude2"><?php echo $marker->getGeocode()->getLatitude(); ?></span>
      </div>
    </div>

    <div class="row">
      <div class="label">
        <label for="marker-geocode-longitude2">Longitude:</label>
      </div>
      <div class="value">
        <span id="marker-geocode-longitude2"><?php echo $marker->getGeocode()->getLongitude(); ?></span>
      </div>
    </div>

  </div>

  <div id="<?php echo $map2->getContainerId(); ?>" class="map-holder" style="width: <?php echo $map2->getWidth(); ?>px; height: <?php echo $map2->getHeight(); ?>px"></div>

  <div class="demo-settings">

  	<h2>Map 3</h2>

    <div class="row">
      <div class="label">
        <label for="marker-geocode-latitude3">Latitude:</label>
      </div>
      <div class="value">
        <span id="marker-geocode-latitude3"><?php echo $marker->getGeocode()->getLatitude(); ?></span>
      </div>
    </div>

    <div class="row">
      <div class="label">
        <label for="marker-geocode-longitude3">Longitude:</label>
      </div>
      <div class="value">
        <span id="marker-geocode-longitude3"><?php echo $marker->getGeocode()->getLongitude(); ?></span>
      </div>
    </div>

    <a id="map-3-toggle" href="#">Toggle the 3th map</a>

  </div>

  <div id="map-3-parent" class="hide-map">
  	<div id="<?php echo $map3->getContainerId(); ?>" class="map-holder" style="width: <?php echo $map3->getWidth(); ?>px; height: <?php echo $map3->getHeight(); ?>px"></div>
  </div>

<?php require_once('../../footer.phtml'); ?>