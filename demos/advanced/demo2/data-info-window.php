<?php

  /**
   * Copyright (c) 2009, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Advanced demo 2
   * @description:  This demo data file retrieves the content for a info window.
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.2
   * @versionDate:  2010-02-07
   * @date:         2010-02-07
   */

  // Including of some sample data
  require_once('../../testdata/data.php');

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/generic/InfoWindowContent.php');

  // Get parameters from URL
  $projectId    = isset($_GET['id']) ? $_GET['id'] : 0;
  $type         = isset($_GET['type']) ? $_GET['type'] : '';

  $infoWindow = new SVZ_Solutions_Generic_Info_Window_Content();

  $infoWindow->addClassName('type-' . strtolower($type));

  // Find the marker in the sample data array by its id
  foreach ($data as $key => $value)
  {
    if ($value['entityId'] == $projectId)
      $result[] = $value;

  }

  $output = array();

  if ($result)
  {
    foreach ($result as $project)
    {
      $projectAddress                 = $project['address'];

      $price                          = '';

      // Defining the way the header looks like
      $headerHtml = '<img class="main-image" alt="" src="../../inc/img/woning.jpg" />
               <h2>' . $project['title'] . '</h2>
               <h3>' . $price . '</h3>';

      $infoWindow->setHeaderHtml($headerHtml);

      $propertiesTab = '<p>Properties</p>';

      $textTab = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla cursus, lacus id rhoncus congue, nunc odio lobortis purus, sed interdum neque sem quis risus. Vivamus neque diam, commodo vitae imperdiet at, sodales tempus leo. Morbi sollicitudin rutrum aliquam. Fusce lacinia mi non erat sagittis a pellentesque augue scelerisque. Duis vitae dui turpis, non euismod urna. Ut lacinia eleifend justo, sed posuere quam ornare pretium. Donec eu fringilla dolor. Pellentesque vestibulum malesuada faucibus. Quisque scelerisque, arcu semper aliquam pharetra, dolor ligula feugiat ante, vel blandit tellus arcu quis erat. Aliquam egestas tempor turpis ac suscipit. Maecenas imperdiet tristique ornare. Donec accumsan ultricies ante, eu molestie lorem fermentum sit amet. Sed vel justo lacus, eu sagittis nulla. Nullam vel libero in arcu consequat malesuada ac nec risus.</p>';

      $textTab .= '<p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus eu est eros, at porttitor sem. Curabitur sit amet sapien magna. Nam sit amet felis sed lectus blandit rutrum. Sed leo urna, mattis eu ornare sed, condimentum eget eros. Donec lobortis sollicitudin mi, porttitor fringilla massa tincidunt sit amet. Pellentesque tincidunt risus vitae velit tempor aliquam. Aenean eu sodales lacus. Aliquam condimentum ligula vel augue iaculis at tristique dui porta. Fusce pellentesque, ipsum vel consectetur sollicitudin, purus mauris scelerisque sem, et placerat eros neque ut justo. Pellentesque vel dui sapien, ut fermentum nisl. Duis in orci diam. In dolor lectus, eleifend eget luctus nec, adipiscing ut ante. Mauris suscipit lacus ac nibh auctor at pretium sapien venenatis. </p>';


      $mediaTab = '';
      $mediaTab .= '<a href="#" class="dialog">&raquo; Toon meer afbeeldingen</a><br />';
      $mediaTab .= '<a href="#" class="dialog">&raquo; Toon videos</a><br />';
      $mediaTab .= '<a href="#" class="dialog">&raquo; Toon plattegronden</a><br />';
      $mediaTab .= '<a href="#" class="dialog">&raquo; Toon 360 graden presentatie</a><br />';

      $actionsTab = '<ul>' .
                      '<li><a href="#">&raquo; Voeg toe aan vergelijking</a></li>' .
                      '<li><a href="#">&raquo; Reageer op dit object</a></li>' .
                      '<li><a href="#">&raquo; Informeer een bekende</a></li>' .
                      '<li><a href="#">&raquo; Print object</a></li>' .
                      '<li><a class="external-link" href="#">&raquo; Reserveer woning</a></li>' .
                      '<li><a class="external-link" href="#">&raquo; Monitor woning</a></li>' .
                    '</ul>';



      $infoWindow->addTab('Kenmerken', $propertiesTab);
      $infoWindow->addTab('Teksten', $textTab);
      $infoWindow->addTab('Media', $mediaTab);
      $infoWindow->addTab('Acties op deze woning', $actionsTab);

      $output['content'] = $infoWindow->getHTML();
    }

  }
  else
  {
    $output['content'] = 'Could not find the project data.';
  }

  echo json_encode($output);

?>