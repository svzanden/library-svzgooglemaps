<?php

  /**
   * Copyright (c) 2009, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Advanced demo 2
   * @description:  This demo data file retrieves a set of data marker positions from the
   *                testdata array and filters them on the types specified.
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.2
   * @versionDate:  2010-02-07
   * @date:         2010-02-07
   */

  // Including of some sample data
  require_once('../../testdata/data.php');

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/generic/MarkerManager.php');

  // Reading out data applied in the requests
  $markerTypes      = isset($_GET['marker-types']) ? $_GET['marker-types'] : array();

  $mapClusterMode   = SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_NONE;

  $markerManager    = new SVZ_Solutions_Generic_Marker_Manager();
  $markerManager->setClusterMode($mapClusterMode);
  $markerManager->import($data);

  // Generate JSON output
  $output           = new StdClass();

  // This filtering should be done when the data is retrieved from the database
  $tempMarkers      = $markerManager->toArray();
  $markers          = array();

  foreach ($tempMarkers as $tempMarker)
  {
    if (in_array(strtolower($tempMarker['type']), $markerTypes))
      $markers[] = $tempMarker;

  }

  $output->markers  = $markers;

  echo json_encode($output);

?>