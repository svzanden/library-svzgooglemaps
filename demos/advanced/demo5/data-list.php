<?php

  /**
   * Copyright (c) 2010, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Advanced demo 4
   * @description:  This demo data file retrieves the content for a info window list.
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.5
   * @versionDate:  2010-04-05
   * @date:         2010-04-05
   */

  // Including of some sample data
  require_once('../../testdata/data.php');


  // Get parameters from URL
  $entityIds            = isset($_POST['entityIds']) ? $_POST['entityIds'] : array();

  $output               = array();

  $content              = '';

  $maxReturned          = 10;
  $i                    = 0;

  foreach ($entityIds as $entityId)
  {
    if ($i == $maxReturned)
      break;

    $entity = null;

    // Find the entity
    foreach ($data as $value)
    {
      if ($value['entityId'] == $entityId)
        $entity = $value;

    }

    if ($entity)
    {
      $content .= '<a href="' . $entity['dataLoadUrl'] . '">';

      $content .= '<img alt="" src="../../inc/img/woning.jpg" width="90" /><br />';

      $content .= $entity['title'];

      $content .= '</a><br /><br />';
    }

    $i++;
  }

  $output['content'] = $content;

  echo json_encode($output);

?>