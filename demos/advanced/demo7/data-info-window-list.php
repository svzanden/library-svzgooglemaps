<?php

  /**
   * Copyright (c) 2010, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Advanced demo 7
   * @description:  This demo data file retrieves the content for a info window list.
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.6.1
   * @versionDate:  2010-10-03
   * @date:         2010-10-03
   */

  require_once('data-info-window.php');

?>