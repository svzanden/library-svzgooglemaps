<?php

  /**
   * Copyright (c) 2010, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Advanced demo 6
   * @description:  This demo shows plotting all markers on a map and using directions.
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.6
   * @versionDate:  2010-08-08
   * @date:         2010-08-08
   */

  session_start();

  // Including of some sample data
  require_once('../../testdata/data.php');

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/maps/Map.php');

  $mapClusterMode               = !empty($_SESSION['map_cluster_mode']) ? $_SESSION['map_cluster_mode'] : SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_NONE;

  $map                          = SVZ_Solutions_Maps_Map::getInstance(SVZ_Solutions_Maps_Map::MAP_TYPE_GOOGLE_MAPS, '3');
  $map->setWidth(700);
  $map->setHeight(500);

  // Sets the id of the container (HTMLDomElement) the map must be put on.
  $map->setContainerId('map');

  // Set the absolute or relative location of the file which will return your data like markers.
  $map->setDataLoadUrl('data.php');

  // Sets the default map type to roadmap.
  $map->setMapType('roadmap');

  // Sets the zoom level to start with to 8.
  $map->setZoomLevel(8);

  // Sets that the map will only requests for the data like markers one time.
  if ($mapClusterMode == SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_NONE)
    $map->setLoadDataOnce(true);

  // Sets the geocode the map should start at centered.
  $map->setCenterGeocode(new SVZ_Solutions_Generic_Geocode(51.5479672, 5.648499));

  // Create for each type a marker type
  foreach ($types as $type)
  {
    // Add a type marker type
    $markerType = new SVZ_Solutions_Generic_Marker_Type($type);
    $map->addMarkerType($markerType);
  }

  // Add a cluster marker type
  $markerType = new SVZ_Solutions_Generic_Marker_Type(SVZ_Solutions_Generic_Marker_Type::MARKER_TYPE_CLUSTER);
  $markerType->setClickAction('zoom');

  $map->addMarkerType($markerType);

  // Add a admin marker type
  $markerType = new SVZ_Solutions_Generic_Marker_Type('admin');
  $markerType->enableIcon();
  $markerType->getLayer()->setName('admin'); // Define a layer where the provided marker types should live in
  $markerType->getLayer()->setTypeStatic();
  $map->addMarkerType($markerType);

  // Add a single admin marker
  $marker     = new SVZ_Solutions_Generic_Marker('admin', 51.5479672, 5.648499);
  $marker->setDraggable(true);
  $map->addMarker($marker);

?>
<!DOCTYPE HTML>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>SVZ Solutions - Maps - Advanced demo 6</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
  <link rel="stylesheet" type="text/css" href="../../../inc/css/svzmaps.css" media="all" />
  <link rel="stylesheet" type="text/css" href="../../inc/css/svzmaps-your.css" media="all" />

  <!--[if lte IE 9]>
  <link rel="stylesheet" type="text/css" href="../../inc/css/svzmaps-your-ie.css" media="all" />
  <![endif]-->

  <!-- BEGIN: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->
  <link rel="stylesheet" type="text/css" href="../../inc/css/demo.css" media="all" />
  <!-- END: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->

  <?php

    $requires = array('yourlib/maps/googlemaps/ExtensionAdvancedDemo6');

    $initOnLoad = "var mapManager  = new svzsolutions.maps.MapManager();

      // The SVZ_Solutions_Maps_Google_Maps_Map php class will generate a config object depending on your settings for you,
      // this generated object can be encoded into a JSON string and can be put encoded into the svzsolutions.maps.MapManager object.
      var map         = mapManager.initByConfig('" . json_encode($map->getConfig()) . "');

      // Initialize your extensions
      var extension   = new yourlib.maps.googlemaps.ExtensionAdvancedDemo6(map);

      // Startup all the maps (call after subscribing within your extensions)
      mapManager.startup();";

  ?>
</head>
<body>

  <div class="demo-information">
    <h1>Advanced demo 6: Plotting all markers on a map and calculating routes between them</h1>
    <p><a href="../../index.html">&#171; back to demo overview</a></p>
    <p>This demo shows plotting all markers on a map and directions.<br />
    More info about this can be found <a href="#">here *TODO*</a>.</p>
  </div>

  <div class="demo-settings">
    <form method="post">

      <div class="row">
        <div class="label">
            <label for="map-cluster-mode">Cluster mode:</label>
        </div>
        <div class="value">
          <select id="map-cluster-mode" name="map_cluster_mode">
            <option value="<?php echo SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_NONE . ($mapClusterMode == SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_NONE ? '" selected="selected' : ''); ?>">None</option>
            <option value="<?php echo SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_DISTANCE . ($mapClusterMode == SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_DISTANCE ? '" selected="selected' : ''); ?>">Distance</option>
            <option value="<?php echo SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_ADDRESS . ($mapClusterMode == SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_ADDRESS ? '" selected="selected' : ''); ?>">Address</option>
          </select>
        </div>
      </div>

      <div class="row">
        <div class="label">
          <label for="map-center-geocode-address-search">Start trip from address:</label>
        </div>
        <div class="value">
          <input type="text" id="map-center-geocode-address-search" name="map-center-geocode-address-search" value="" /> <span id="map-center-geocode-address-found"></span>
        </div>
      </div>

      <div class="row">
        <div class="label">
          <label for="map-travel-mode">Travel mode:</label>
        </div>
        <div class="value">
          <select id="map-travel-mode" name="map-travel-mode">
            <?php

              $defaultTravelMode  = 'DRIVING';
              $travelModes        = array('BICYCLING' => 'Bicycling', 'DRIVING' => 'Driving', 'WALKING' => 'Walking');

              foreach ($travelModes as $key => $value)
              {
                $selected = '';

                if ($defaultTravelMode == $key)
                  $selected = ' selected="selected"';

                echo '<option value="' . $key . '"' . $selected . '>' . $value . '</option>';
              }

            ?>
          </select>
        </div>
      </div>

      <div class="row">
        <div class="label">
          <label for="map-avoid-highways">Avoid highways:</label>
        </div>
        <div class="value">
          <input type="checkbox" id="map-avoid-highways" name="map-avoid-highways" value="" />
        </div>
      </div>

      <div class="row">
        <div class="label">
          <label for="map-avoid-tolls">Avoid toll roads:</label>
        </div>
        <div class="value">
          <input type="checkbox" id="map-avoid-tolls" name="map-avoid-tolls" value="" />
        </div>
      </div>

      <div class="row">
        <div class="label">
          <label for="map-provide-route-alternatives">Provide route alternatives:</label>
        </div>
        <div class="value">
          <input type="checkbox" id="map-provide-route-alternatives" name="map-provide-route-alternatives" value="" />
        </div>
      </div>
    </form>
  </div>

  <div class="sg-steps-holder">
    <div id="sg-steps"></div>
  </div>

  <div id="<?php echo $map->getContainerId(); ?>" class="map-holder" style="width: <?php echo $map->getWidth(); ?>px; height: <?php echo $map->getHeight(); ?>px"></div>

<?php require_once('../../footer.phtml'); ?>