<?php

  /**
   * Copyright (c) 2010, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Advanced demo 6
   * @description:  This demo data file retrieves a set of data marker positions from the
   *                testdata array and filters them on the types specified.
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.6
   * @versionDate:  2010-08-08
   * @date:         2010-08-08
   */

  session_start();

  // Including of some sample data
  require_once('../../testdata/data.php');

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/generic/MarkerManager.php');

  // Reading out data applied in the requests
  $markerTypes      = isset($_GET['marker-types']) ? $_GET['marker-types'] : array();

  // Reading out data applied in the requests
  $mapClusterMode   = !empty($_SESSION['map_cluster_mode']) ? $_SESSION['map_cluster_mode'] : SVZ_Solutions_Generic_Marker_Manager::CLUSTER_MODE_NONE;

  $markerManager    = new SVZ_Solutions_Generic_Marker_Manager();
  $markerManager->setClusterMode($mapClusterMode);
  $markerManager->import($data);
  $markerManager->disableListMarker(true);

  // Generate JSON output
  $output           = new StdClass();

  $output->markers  = $markerManager->toArray();

  echo json_encode($output);

?>