<?php

  /**
   * Copyright (c) 2009, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Advanced demo 4
   * @description:  This demo data file retrieves the content for a info window.
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.4
   * @versionDate:  2010-02-28
   * @date:         2010-02-28
   */

  // Including of some sample data
  require_once('../../testdata/data.php');


  // Get parameters from URL
  $entityIds    = isset($_GET['entityIds']) ? explode(',', $_GET['entityIds']) : array();
  $type         = isset($_GET['type']) ? $_GET['type'] : '';

  $mode         = 'normal';

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/generic/InfoWindowContent.php');

  // Get parameters from URL
  $projectId    = isset($_GET['id']) ? $_GET['id'] : 0;
  $type         = isset($_GET['type']) ? $_GET['type'] : '';

  $infoWindow = new SVZ_Solutions_Generic_Info_Window_Content();

  $infoWindow->addClassName('type-' . strtolower($type));


  // Find the marker in the sample data array by its id
  foreach ($data as $key => $value)
  {
    if ($value['entityId'] == $projectId)
      $result[] = $value;

  }

  $output = array();

  if ($result)
  {
    foreach ($result as $project)
    {
      $projectAddress                 = $project['address'];

      $price                          = '';

      // Defining the way the header looks like
      $headerHtml = '<img class="main-image" alt="" src="../../inc/img/woning.jpg" />
               <h2>' . $project['title'] . '</h2>
               <h3>' . $price . '</h3>';

      $infoWindow->setHeaderHtml($headerHtml);

      $actionsTab = '<p>Actions</p>';

      $actionsTab = '<a class="action-add-marker-to-route" href="#">Add to route</a>';

      $infoWindow->addTab('Actions', $actionsTab);

      $output['content'] = $infoWindow->getHTML();
    }

  }
  else
  {
    $output['content'] = 'Could not find the project data.';
  }

  echo json_encode($output);

?>