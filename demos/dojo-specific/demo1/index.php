<?php

  /**
   * Copyright (c) 2010, SVZ Solutions All Rights Reserved.
   * Available via BSD license, see license file included for details.
   *
   * @title:        SVZ Solutions Dojo specific demo 1
   * @description:  Demo showing the TabContainer and Contentpane loading a map through Ajax
   * @authors:      Stefan van Zanden <info@svzsolutions.nl>
   * @company:      SVZ Solutions
   * @contributers:
   * @version:      0.5
   * @versionDate:  2010-04-25
   * @date:         2010-04-25
   */

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/maps/Map.php');

  $map                          = SVZ_Solutions_Maps_Map::getInstance(SVZ_Solutions_Maps_Map::MAP_TYPE_GOOGLE_MAPS, '3');
  $map->setWidth(700);
  $map->setHeight(400);

  // Sets the id of the container (HTMLDomElement) the map must be put on.
  $map->setContainerId('map');

  // Sets the default map type to roadmap.
  $map->setMapType('roadmap');

  // Sets the zoom level to start with to 8.
  $map->setZoomLevel(8);

  // Sets the geocode the map should start at centered.
  $map->setCenterGeocode(new SVZ_Solutions_Generic_Geocode(51.5479672, 5.648499));

  // Add a admin marker type
  $markerType = new SVZ_Solutions_Generic_Marker_Type('admin');
  $markerType->enableIcon();
  $markerType->getLayer()->setName('admin'); // Define a layer where the provided marker types should live in
  $markerType->getLayer()->setTypeFixed();
  $map->addMarkerType($markerType);

  // Add a single admin marker
  $marker     = new SVZ_Solutions_Generic_Marker('admin', 51.5479672, 5.648499);
  $marker->setDraggable(true);
  $marker->setContent('Drag me!');
  $map->addMarker($marker);

?>
<!DOCTYPE HTML>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>SVZ Solutions - Maps - Dojo specific demo 1</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
  <link rel="stylesheet" type="text/css" href="../../../inc/css/svzmaps.css" media="all" />
  <link rel="stylesheet" type="text/css" href="../../inc/css/svzmaps-your.css" media="all" />
  <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/dojo/1.5.0/dijit/themes/tundra/tundra.css" />

  <!--[if lte IE 9]>
  <link rel="stylesheet" type="text/css" href="../../inc/css/svzmaps-your-ie.css" media="all" />
  <![endif]-->

  <!-- BEGIN: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->
  <link rel="stylesheet" type="text/css" href="../../inc/css/demo.css" media="all" />
  <!-- END: LEAVE THIS FILE OUT, ONLY FOR DEMO PURPOSES -->

  <?php

    $requires = array('dojo/parser', 'dijit/layout/TabContainer', 'dijit/layout/ContentPane');

    $initOnLoad = "dojo.parser.parse();

      var mapContentPane = dijit.byId('mapContentPane');

      mapContentPane.attr('onDownloadEnd', function() {

      	var mapManager   = new svzsolutions.maps.MapManager();

        // The SVZ_Solutions_Maps_Google_Maps_Map php class will generate a config object depending on your settings for you,
        // this generated object can be encoded into a JSON string and can be put encoded into the svzsolutions.maps.MapManager object.
        var map         = mapManager.initByConfig('" . json_encode($map->getConfig()) . "');

        // Startup all the maps (call after subscribing within your extensions)
        mapManager.startup();
      });";

  ?>
</head>
<body class="tundra">

  <div class="demo-information">
    <h1>Dojo specific demo 1: Loading a map through Ajax in the Dojo TabContainer and Contentpane</h1>
    <p><a href="../../index.html">&#171; back to demo overview</a></p>
    <p>This demo shows a Dojo TabContainer and Contentpane loading a map through Ajax.</p>
  </div>

  <div style="width: 800px; height: 500px">
    <div id="mainTabContainer" dojoType="dijit.layout.TabContainer" style="width: 100%; height: 100%;">
        <div dojoType="dijit.layout.ContentPane" title="My first tab" selected="true">
          Lorem ipsum and all around
        </div>
        <div dojoType="dijit.layout.ContentPane" title="My second tab with map">

          <div id="mapContentPane" dojoType="dijit.layout.ContentPane" href="ajax-map.php">
            Hi, pretty boring huh?
          </div>

        </div>
        <div dojoType="dijit.layout.ContentPane" title="My last tab" closable="true">
            Lorem ipsum and all around - last...
        </div>
    </div>
  </div>

<?php require_once('../../footer.phtml'); ?>