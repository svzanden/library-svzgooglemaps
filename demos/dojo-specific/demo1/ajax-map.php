<?php

  // Including of the SVZ Solutions library
  require_once('../../../includes/svzsolutions/maps/Map.php');

  $map                          = SVZ_Solutions_Maps_Map::getInstance(SVZ_Solutions_Maps_Map::MAP_TYPE_GOOGLE_MAPS, '3');
  $map->setWidth(700);
  $map->setHeight(400);

  // Sets the id of the container (HTMLDomElement) the map must be put on.
  $map->setContainerId('map');

?>

<div id="<?php echo $map->getContainerId(); ?>" class="map-holder" style="width: <?php echo $map->getWidth(); ?>px; height: <?php echo $map->getHeight(); ?>px"></div>